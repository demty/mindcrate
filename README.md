# NB

Это пример сервиса и он не может быть запущен, он отражает мои навыки.

Сервис полностью разработан мною, в проекте можно проследить изменение подхода к разработке.

services.admitad - пример работы с бд

apps.telegram - интеграция через webhook с телеграммом

apps.integrator.operations.transfer_segment - пример работы с использованием runnector

external_services - примеры работы с внешними апи

internal_services - пример работы с внутренним сервисом

P.S.: Много кода удалено, чтобы не создавать лишний шум, оставил только то, что отражает основные функции микросервиса
# Сервис интеграции с майндбоксом
## Развертывание
```
virtualenv -p python3.7 venv
source ./venv/bin/activate

pip install -r requirements
```
## Запуск
```
python main.py
```
## Миграции
```
pgmigrate -d migrations -c "host= port= dbname= user= password=" -t latest migrate
```
## Что за сервис
Сервис для агрегации и последующей интеграции данных сайта в различные маркетинговые сервисы

Интегрирован в:
* RTB
* Mindbox
* Criteo
* Google analytics
* Admitad
