try:
    import uvloop
except ImportError:
    pass
import platform
import asyncio
import logging
import logging.config
import uuid
# import os

import aiotask_context as context
import settings
from aiohttp import web
# from apps.tracker.tasks import check_for_finished_orders
from routes import setup_routes
from utils.app_db import close_db, init_db
from utils.app_lobster_config import init_lobster_config, close_config
from utils.app_session import close_session, init_session
from utils.middlewares import x_request_id_middleware

logger = logging.getLogger(__name__)


def init(loop, test_call: bool=False):
    """Инициализация аппа"""
    app = web.Application(loop=loop, middlewares=[x_request_id_middleware], client_max_size=1024 * 1024 * 2)
    app['guid'] = str(uuid.uuid4())
    logger.debug(f"app_guid: {app['guid']}")
    setup_routes(app)
    app.test_call = test_call
    app.on_startup.append(initialize_app_vars)
    app.on_cleanup.append(destroy_app_vars)
    return app


async def initialize_app_vars(app):
    """Инициализация всяких штук для app(сессии, пул бд и тд)"""
    await init_db(app)
    await init_session(app)
    await init_lobster_config(app)
    # if not app.test_call:
    #     await init_scheduler(app=app, tasks=[check_for_finished_orders])


async def destroy_app_vars(app):
    """Уничтожение всяких штук для app(сессии, пул бд и тд)"""
    await close_db(app)
    await close_session(app)
    await close_config(app)
    # if not app.test_call:
    #     await stop_scheduler(app)


def main():
    """Главный апп"""
    logging.config.dictConfig(settings.LOGGING)
    if platform.system() != 'Windows':
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    loop.set_task_factory(context.task_factory)
    app = init(loop=loop)
    web.run_app(app, host=settings.APP_HOST, port=settings.APP_PORT)


if __name__ == '__main__':
    main()
