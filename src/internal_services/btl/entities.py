from typing import Dict


class RegisterUserInfo:
    """ДТО данных пользвоателя"""

    def __init__(self, data: Dict):
        """Конструктор"""
        self._data = data

    def get_email(self) -> str:
        """Получить емейл пользователя"""
        return self._data['email']
