class NoRegisterData(Exception):
    """Не найдены данные регистрации"""

    pass


class CantGetRegisterData(Exception):
    """Не удалось получить данные регистрации"""

    pass
