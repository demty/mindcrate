from aiohttp.client import ClientSession
from internal_services.btl.controller import BtlController
from internal_services.btl.repository import BtlRepository
from internal_services.btl.service import BtlService


class ControllerFactory:
    """Фабрика контроллеров"""

    @classmethod
    def get_controller(cls, session: ClientSession) -> BtlController:
        """Получить контроллер"""
        repo = BtlRepository(session=session)
        service = BtlService(repository=repo)
        cont = BtlController(service=service)
        return cont
