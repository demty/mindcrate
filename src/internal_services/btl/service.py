from internal_services.btl.entities import RegisterUserInfo
from internal_services.btl.repository import BtlRepository


class BtlService:
    """Сервис бтла"""

    def __init__(self, repository: BtlRepository):
        """Конструктор"""
        self._repo = repository

    async def get_user_email(self, user_id: str) -> RegisterUserInfo:
        """Получить емейл пользователя"""
        return RegisterUserInfo(data=(await self._repo.get_user_email(user_id=user_id)).get('data', dict()))
