import logging
from typing import Dict

from aiohttp.client import ClientSession
from internal_services.btl.exceptions import CantGetRegisterData, NoRegisterData
from settings import BTL_API_ENDPOINT, BTL_TOKEN

logger = logging.getLogger(__name__)


class BtlRepository:
    """Репозиторий бтл"""

    def __init__(self, session: ClientSession):
        """Конструктор"""
        self._session = session

    async def get_user_email(self, user_id: str) -> Dict:
        """Получить емейл юзера и еще немного данных"""
        async with self._session.get(f'{BTL_API_ENDPOINT}/1.0/enrg/registration/{user_id}',
                                     headers={'token': BTL_TOKEN}) as resp:
            if resp.status == 200:
                return await resp.json()
            elif resp.status == 404:
                raise NoRegisterData("Пользователь не указывал свой емейл")
            else:
                logger.exception({'msg': 'Не удалось получить данные пользователя', 'data': {'user_id': user_id},
                                  'text': await resp.text()})
                raise CantGetRegisterData('Не удалось получить данные регистрации')
