from internal_services.btl.entities import RegisterUserInfo
from internal_services.btl.service import BtlService


class BtlController:
    """Контроллер бтда"""

    def __init__(self, service: BtlService):
        """Конструктор"""
        self._service = service

    async def get_user_email(self, user_id: str or int) -> RegisterUserInfo:
        """Получить дто с емейлом юзера"""
        return await self._service.get_user_email(user_id=user_id)
