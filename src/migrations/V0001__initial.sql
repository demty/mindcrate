CREATE TABLE public.admitad_tracks (
       id SERIAL PRIMARY KEY,
       order_number varchar(50) unique not null,
       order_guid varchar(50) unique not null,
       status varchar(50) not null,
       status_updated_at timestamp not null,
       submit boolean NOT NULL DEFAULT FALSE
);
