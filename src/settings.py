import logging
from envparse import env
from utils.log_filters import RequestIdLogFilter
from schema import Or

env.read_envfile()

ENV = env('RUN_ENV', '')

DEBUG = ENV != 'prod'

log_level_names = [
    logging.getLevelName(level)
    for level in (logging.CRITICAL,
                  logging.ERROR,
                  logging.WARNING,
                  logging.INFO,
                  logging.DEBUG,
                  logging.NOTSET)
]
error_log_level = logging.getLevelName(logging.ERROR)


def get_log_level_from_env(var_name, default):
    validator = Or(*log_level_names,
                   error=f'{var_name} failed. "{{}}" not in {log_level_names}')
    return validator.validate(env(var_name, default))


LOGGING_LEVEL = env('LOG_LEVEL', error_log_level)

APP_HOST = env('APP_HOST', '0.0.0.0')
APP_PORT = env('APP_PORT', cast=int, default=80)

SENTRY_DSN = env('SENTRY_DSN', '')

# region database

DATABASE_NAME = env('POSTGRES_DB_NAME')
DATABASE_USER = env('POSTGRES_DB_USER')
DATABASE_PASSWORD = env('POSTGRES_DB_PASSWORD')
DATABASE_HOST = env('POSTGRES_DB_HOST')
DATABASE_PORT = env('POSTGRES_DB_PORT')
DB_POOL_CONNECTIONS_MIN_SIZE = env('POSTGRES_DB_POOL_CONNECTIONS_MIN_SIZE', default=1, cast=int)
DB_POOL_CONNECTIONS_MAX_SIZE = env('POSTGRES_DB_POOL_CONNECTIONS_MAX_SIZE', default=1, cast=int)

# endregion

MIN_LIMIT = env('MIN_LIMIT', cast=int, default=20)
MIN_OFFSET = env('MIN_OFFSET', cast=int, default=0)

MAX_LEVEL = env('MAX_LEVEL', cast=int, default=99)

ORDER_API_ENDPOINT = env('ORDER_API_ENDPOINT', '').rstrip('/')
ENNERGIIA_ENDPOINT_EXTERNAL = env('ENNERGIIA_ENDPOINT_EXTERNAL', 'mock').rstrip('/')

# region mindbox

MINDBOX_DEFAULT_BRAND = 'mock'
MINDBOX_ENDPOINT_EXTERNAL = env('MINDBOX_ENDPOINT_EXTERNAL', '').rstrip('/')
MINDBOX_MODE = env('MINDBOX_MODE', 'mock')
MINDBOX_MODE_REPO = 'production' if MINDBOX_MODE in ['test', 'production'] else 'mock'
MINDBOX_ACCESS_KEY = env('MINDBOX_ACCESS_KEY', '')
ORDER_POSITION_STATUS_STOPLIST = ['overdue']

# endregion

# region admitad

ADMITAD_ENDPOINT_EXTERNAL = env('ADMITAD_ENDPOINT_EXTERNAL', 'mock').rstrip('/')
ADMITAD_CAMPAIGN_CODE = env('ADMITAD_CAMPAIGN_CODE', '')
ADMITAD_REVISION_KEY = env('ADMITAD_REVISION_KEY', '')
ADMITAD_MODE = env('ADMITAD_MODE', 'debug')
ADMITAD_TERMINAL_WAIT_TIME_DAYS = 14
ADMITAD_CHECK_JOB_EVERY_SECONDS = 43200  # 12 hours

# endregion

BTL_TOKEN = env('BTL_TOKEN', '')
BTL_API_ENDPOINT = env('BTL_API_ENDPOINT', '').rstrip('/')

AUTH_API_ENDPOINT = env('AUTH_API_ENDPOINT', '').rstrip('/')
AUTH_API_SERVICE_LOGIN = env('AUTH_API_SERVICE_LOGIN', 'mock')
AUTH_API_SERVICE_PASSWORD = env('AUTH_API_SERVICE_PASSWORD', '')
AUTH_API_SERVICE_REFERRER = env('AUTH_API_SERVICE_REFERRER', 'mock')
AUTH_API_SERVICE_SLUG = env('AUTH_API_SERVICE_SLUG', 'mock')
AUTH_API_SERVICE_SECRET = env('AUTH_API_SERVICE_SECRET', '')

WISHLIST_API_ENDPOINT = env('WISHLIST_API_ENDPOINT', '').rstrip('/')

ALLIGATOR_API_ENDPOINT = env('ALLIGATOR_API_ENDPOINT', '').rstrip('/')

BASE_SVYABK = 'mock'
BASE_AVAIL_ZONE = 0
BASE_PRICE_ZONE = 0

# region massmedia

MASSMEDIA_API_ENDPOINT = env('MASSMEDIA_API_ENDPOINT', '').rstrip('/')
MASSMEDIA_TOKEN = env('MASSMEDIA_TOKEN', '')

# endregion

# region sap

SAP_LOGIN = env('SAP_LOGIN', '')
SAP_PASSWORD = env('SAP_PASSWORD', '')
SAP_API_ENDPOINT = env('SAP_API_ENDPOINT', '').rstrip('/')

# endregion

# region Google Analytics

GOOGLE_ANALYTICS_ENDPOINT_EXTERNAL = env('GOOGLE_ANALYTICS_ENDPOINT_EXTERNAL', '').rstrip('/')
GOOGLE_ANALYTICS_TRANSACTION_TID = env('GOOGLE_ANALYTICS_TRANSACTION_TID', 'mock')

# endregion

# region redis

REDIS_HOST = env('REDIS_HOST', '')
REDIS_PORT = env('REDIS_PORT', '6379')
REDIS_DB_NAME = env('REDIS_DB_NAME', '')

REDIS_TASK_EXPIRE_TIME = env('REDIS_TASK_EXPIRE_TIME', 600)  # 10 минут

# endregion

# region telegram

BOT_TOKEN = env('BOT_TOKEN', '')

# endregion

# region criteo

CRITEO_API_ENDPOINT = env('CRITEO_API_ENDPOINT_EXTERNAL', '').rstrip('/')
CRITEO_CLIENT_ID = env('CRITEO_CLIENT_ID', '')
CRITEO_CLIENT_SECRET = env('CRITEO_CLIENT_SECRET', '')
CRITEO_ADVERTISER_ID = env.int('CRITEO_ADVERTISER_ID', 0)

# endregion

# region RTB

RTB_API_ENDPOINT = env('RTB_API_ENDPOINT_EXTERNAL', '').rstrip('/')
RTB_COMPANY_HASH = env('RTB_COMPANY_HASH', '')

# endregion

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(process)d %(name)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'json_verbose_with_request_id': {
            'class': 'utils.formatters.JsonFormatter',
            'format':
                '{"logtime": "%(asctime)s", "loglevel": "%(levelname)s", "request_id" :"%(request_id)s",'
                ', "filename": "%(filename)s, "funcName": "%(funcName)s", "lineno": "%(lineno)s",'
                '"logger": "%(name)s", "extra": %(message)s}'
        },
        'json_verbose_without_request_id': {
            'class': 'utils.formatters.JsonFormatter',
            'format':
                '{"logtime": "%(asctime)s", "loglevel": "%(levelname)s", "filename": "%(filename)s, '
                '"logger": "%(name)s", "funcName": "%(funcName)s", "lineno": "%(lineno)s", "extra": %(message)s}'
        },
    },
    'handlers': {
        'console': {
            'level': LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'json_verbose_without_request_id' if ENV != 'local' else 'simple'
        },
        'console_with_request_id': {
            'level': LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'json_verbose_with_request_id' if ENV != 'local' else 'simple',
            'filters': ['requestid'],
        },
        'sentry': {
            # ERROR
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
        },
        'sentry_info': {
            # INFO
            'level': 'INFO',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
        },
        'console_json_with_request_id': {
            'level': 'DEBUG' if DEBUG else LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'json_verbose_with_request_id',
            'filters': ['requestid'],
        },
        'console_json_without_request_id': {
            'level': 'DEBUG' if DEBUG else LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'json_verbose_without_request_id',
        },
        'sentry_all': {
            'level': 'DEBUG',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
        },
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'] if DEBUG else ['console', 'sentry'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'aiohttp.access': {
            'handlers': ['console_with_request_id'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'newrelic': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'sentry': {
            'handlers': ['sentry'],
            'level': 'ERROR',
            'propagate': False
        }
    },
    'filters': {
        'requestid': {
            '()': RequestIdLogFilter,
        },
    },
}
