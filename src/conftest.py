import asyncio

import pytest
from aiohttp.test_utils import loop_context
import aiotask_context as context
from aresponses import ResponsesMockServer


try:
    import uvloop
except ImportError:
    uvloop = asyncio
from main import init

pytest_plugins = [
    'external_services.rtb.tests.fixtures',
    'external_services.criteo.tests.fixtures',
    'external_services.mindbox.tests.fixtures',
    'apps.integrator.operations.transfer_segment.tests.fixtures'
]


@pytest.fixture
def loop():
    loop = uvloop.new_event_loop()
    loop.set_task_factory(context.task_factory)
    asyncio.set_event_loop(loop)
    yield loop
    loop.close()


@pytest.yield_fixture
def loop():
    with loop_context() as _loop:
        yield _loop


@pytest.yield_fixture
def event_loop(loop):
    yield loop


@pytest.fixture
def app(loop):
    _app = init(loop=loop, test_call=True)
    yield _app


@pytest.fixture
def app_server(loop, app, test_server):
    return loop.run_until_complete(test_server(app))


@pytest.fixture
def app_client(loop, app, test_client):
    return loop.run_until_complete(test_client(app))


@pytest.fixture
async def aresponses(loop):
    async with ResponsesMockServer(loop=loop) as server:
        yield server
