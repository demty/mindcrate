from typing import Optional

import asyncpgsa
import asyncpg
import settings
import ujson


def ujson_encoder(value):
    """Энкодер"""
    return value


def ujson_decoder(value):
    """Декодер"""
    return ujson.loads(value)


async def init_connection(connection):
    """Штука для инициализации соединения"""
    await connection.set_type_codec(
        'jsonb', encoder=ujson_encoder, decoder=ujson_decoder, schema='pg_catalog')


async def init_db(app):
    """Открыть соединение с бд"""
    # postgresql://bot:1111@localhost:5432/mindcrate
    app.pool = await asyncpgsa.create_pool(
        dsn=f'postgresql://{settings.DATABASE_USER}:{settings.DATABASE_PASSWORD}@{settings.DATABASE_HOST}:'
            f'{settings.DATABASE_PORT}/{settings.DATABASE_NAME}',
        init=init_connection,
        min_size=settings.DB_POOL_CONNECTIONS_MIN_SIZE,
        max_size=settings.DB_POOL_CONNECTIONS_MAX_SIZE
    )


async def close_db(app):
    """Закрыть соединение с бд"""
    try:
        await app.pool.close()
    except Exception:
        pass
    app.pool: Optional[asyncpg.pool.Pool] = None
    # raise NotImplementedError
