import logging

import ujson
from utils.exceptions import BaseAppException


class JsonFormatter(logging.Formatter):
    """Форматирование лога в json"""

    def format(self, record):
        """
        Отфарматировать переданное сообщение

        :type record: LogRecord
        """
        if not isinstance(record.msg, str) and (type(record.msg) is not dict and record.msg.args):
            if issubclass(type(record.msg), BaseAppException):
                record.msg = ujson.dumps(record.msg.args[0])
            elif issubclass(type(record.msg), Exception):
                record.msg = ujson.dumps(record.msg.args[0])
        else:
            record.msg = ujson.dumps(record.msg)
        return super(JsonFormatter, self).format(record)
