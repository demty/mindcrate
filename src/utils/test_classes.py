import asyncio

import aiotask_context as context
from aiohttp.test_utils import AioHTTPTestCase
from main import init


class MainTestClassAsync(AioHTTPTestCase):
    """Базовый асинхронный тестовый класс"""

    async def get_application(self):
        """Получить приложение"""
        loop = asyncio.get_event_loop()
        loop.set_task_factory(context.task_factory)
        return init(loop)

    @classmethod
    def setUpClass(cls):
        """Фикстура запуска тестового класса"""
        pass

    @classmethod
    def tearDownClass(cls):
        """Фикстура закрытия тестового класса"""
        pass
