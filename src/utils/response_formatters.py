from typing import Dict

import ujson
from aiohttp import web
from utils.exceptions import BaseAppException


def custom_dump(*args, **kwargs):
    """Кастомный сериализатор, который умеет в datetime"""
    return ujson.dumps(*args, **kwargs)


def json_response(data, status):
    """
    Ответ сервиса в виде json

    :param data: данные
    :type data: dict

    :param status: код ответа
    :type status: int
    """
    return web.json_response(data, status=status, dumps=custom_dump)


def ujson_response(data, status):
    """
    Ответ сервиса в виде json (Decimal умеет серилизовать)

    :param data: данные
    :type data: dict

    :param status: код ответа
    :type status: int
    """
    return web.json_response(data, status=status, dumps=ujson.dumps)


def _get_common_error_obj(error) -> Dict:
    error_text = 'Необработанная ошибка'
    if hasattr(error, 'args') and error.args and type(error.args[0]) == str:
        error_text = error.args[0]
    return dict(
        code=500,
        reason=error.__class__.__name__,
        message=error_text
    )


def _convert_error(error):
    if isinstance(error, BaseAppException):
        return {
            'code': error.code,
            'reason': error.reason,
            'message': error.message,
            **error.addon
        }
    else:
        return _get_common_error_obj(error)


def google_json_response(api_version, status, data=None, error=None):
    """
    Ответ сервиса в виде json в google стандарте

    :param api_version: версия апи
    :type api_version: str

    :param data: данные
    :type data: dict

    :param status: код ответа
    :type status: int

    :param error: ошибка
    :type error: None or Exception
    """
    response = {
        'apiVersion': api_version,
    }
    if data:
        response['data'] = data
    elif error:
        response['error'] = _convert_error(error)
    else:
        pass

    return web.json_response(response, status=status, dumps=custom_dump)
