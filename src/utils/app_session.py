from aiohttp.client import ClientSession


async def init_session(app):
    """Установить сессию"""
    app.session = ClientSession()
    app.tg_token = {}


async def close_session(app):
    """Закрыть сессию"""
    await app.session.close()
