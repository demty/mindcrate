import asyncio
import logging
import os

import aioredis
import settings

logger = logging.getLogger(__name__)


def _modify_task_name(task_name: str):
    """Чтобы не пересечься с кем-нибудь еще добавим приставку"""
    return f'mindcrate_{task_name}'


async def check_if_task_can_be_taken(task_name: str, app_guid: str, ttl: int=settings.REDIS_TASK_EXPIRE_TIME):
    """Проверить выполняется ли таск другим инстансом"""
    result = False
    task_name = _modify_task_name(task_name)
    try:
        loop = asyncio.get_event_loop()
        redis = await aioredis.create_redis(
            f'redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB_NAME}',
            loop=loop)
        try:
            flag = await redis.get(task_name)
            if not flag:
                if os.name != 'nt':
                    await redis.set(key=task_name, value=app_guid, expire=ttl)
                else:
                    # Версия редиса для винды (2.4) не поддерживает доп параметры в SET, только с 2.6
                    await redis.set(key=task_name, value=app_guid)
                logger.info(f'Task {task_name} taken {app_guid}')
                result = True
            elif flag == app_guid:
                result = True
                logger.info(f'Task {task_name} still belongs to me! ({flag})')
            else:
                logger.info(f'Task {task_name} already taken by instance {flag}')
        except Exception as e:
            logger.exception(e)
        finally:
            redis.close()
            await redis.wait_closed()
    except Exception as e:
        logger.exception(e)
    return result


async def release_task(task_name: str, app_guid: str):
    """Отпустить таск"""
    try:
        task_name = _modify_task_name(task_name)
        loop = asyncio.get_event_loop()
        redis = await aioredis.create_redis(
            f'redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB_NAME}',
            loop=loop)
        flag = await redis.get(task_name)
        if flag and flag.decode('utf-8') == app_guid:
            await redis.delete(task_name)
        redis.close()
        await redis.wait_closed()
    except Exception as e:
        logger.exception(e)


async def clear_task(task_name: str):
    """Очистить таск, если были записи"""
    try:
        task_name = _modify_task_name(task_name)
        loop = asyncio.get_event_loop()
        redis = await aioredis.create_redis(
            f'redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB_NAME}',
            loop=loop)
        await redis.delete(task_name)
        redis.close()
        await redis.wait_closed()
    except Exception as e:
        logger.exception(e)
