import logging
from asyncio import CancelledError
from functools import wraps

from utils.exceptions import BaseAppException
from utils.response_formatters import google_json_response


def async_error_processing_with_custom_version(logger, log_level='INFO', api_version='1.0'):
    """
    Обработать ошибку view по гугловски(с кастомной версией)

    :type logger: logging.Logger

    :type log_level: уровень логирования бизнес ошибок

    :param api_version: версия апи, пока так, хз как сделать лучше
    :type api_version: str
    """
    def inner(func):
        @wraps(func)
        async def decorated_function(*args, **kwargs):
            try:
                result = await func(*args, **kwargs)
                return result
            except BaseAppException as e:
                logger.log(logging.getLevelName(log_level), e.args[0])
                return google_json_response(error=e, status=200, api_version=api_version)
            except CancelledError as e:
                return google_json_response(error=e, status=500, api_version=api_version)
            except Exception as e:
                logger.exception(e)
                return google_json_response(error=e, status=500, api_version=api_version)
        return decorated_function
    return inner


def async_error_processing(logger, log_level='INFO'):
    """
    Обработать ошибку во view с версионированием и google json standart
    с версией из урла(в урле должно быть {api_version})

    :type logger: logging.Logger

    :type log_level: str

    """
    def decorator(fn):
        @wraps(fn)
        async def wrapper(request, *args, **kwargs):
            api_version = request.match_info.get('api_version')
            try:
                return await fn(request, *args, **kwargs)
            except BaseAppException as error:
                logger.log(logging.getLevelName(log_level), error)
                return google_json_response(api_version=api_version, status=200, error=error)
            except CancelledError as error:
                return google_json_response(api_version=api_version, status=500, error=error)
            except Exception as error:
                logger.exception(error)
                return google_json_response(api_version=api_version, status=500, error=error)
        return wrapper
    return decorator
