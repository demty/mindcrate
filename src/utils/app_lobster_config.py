import settings
from lobster_client.entities import LobsterConfig


async def init_lobster_config(app):
    """Установить сессию"""
    config = LobsterConfig(
        api_url=settings.AUTH_API_ENDPOINT,
        microservice_login=settings.AUTH_API_SERVICE_LOGIN,
        microservice_password=settings.AUTH_API_SERVICE_PASSWORD,
        lobster_secret=settings.AUTH_API_SERVICE_SECRET,
        referrer=settings.AUTH_API_SERVICE_REFERRER,
        service_slug=settings.AUTH_API_SERVICE_SLUG
    )
    app.lobster_config = config


async def close_config(app):
    """Закрыть сессию"""
    app.lobster_config = None
