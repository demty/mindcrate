import logging
from typing import Callable, List

from aiohttp.web import Application
from utils.scheduler import scheduler
from utils.tasks_synchronizer import clear_task

logger = logging.getLogger('scheduler')


async def init_scheduler(app: Application, tasks: List[Callable]):
    """Запустить планировщик"""
    app['scheduler'] = list()
    for task in tasks:
        app['scheduler'].append(app.loop.create_task(scheduler(app=app, task=task)))
        await clear_task(task_name=task.__name__)
        logger.info(f'Task {str(task)} added to schedule')


async def stop_scheduler(app: Application):
    """Оставить планировщик"""
    for item in app['scheduler']:
        item.cancel()
        await item
        logger.info(f'Task {str(item)} stopped')
