import logging

import aiotask_context as context


class RequestIdLogFilter(logging.Filter):
    """Фильтр для X-Request-ID"""

    def filter(self, log_record):
        """Реализация фильтра"""
        log_record.request_id = context.get("X-Request-ID")
        return True
