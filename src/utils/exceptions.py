import logging


logger = logging.getLogger(__name__)


class BaseAppException(Exception):
    """
    Базовый класс для исключений.
    Идея в использовании логгирования при возбуждении исключения.
    Можно задать log_level
    """

    code = 500
    reason = 'commonError'
    internal_code = '500-1'
    addon = {}

    def __init__(self, message, code=None, addon=None, **kwargs):
        """
        :param message: Сообщение
        :type message: str

        :param code: Код ответа
        :type code: int

        :param addon: Дополнительные сведения об исключительной ситуации
        :type code: dict

        :type kwargs: dict
        """
        self.message = message + '. ' + ';'.join(': '.join((str(k), str(v))) for k, v in kwargs.items())
        self.reason = self.__class__.__name__
        if addon:
            self.addon = addon
        logger.debug(self.message)
        if code:
            self.code = code
