import uuid

import aiotask_context as context


async def x_request_id_middleware(app, handler):
    """X-Request-ID middleware"""
    async def middleware_handler(request):
        if not app.test_call:
            context.set("X-Request-ID", request.headers.get("X-Request-ID".lower(), str(uuid.uuid4())))
        response = await handler(request)
        if not app.test_call:
            response.headers["X-Request-ID"] = context.get("X-Request-ID")
        return response
    return middleware_handler
