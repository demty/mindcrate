from typing import Dict, Any


def int_gen(seq: int):
    return seq


def str_gen(seq: int):
    return f'some_str_{seq}'


def gen_fields(fields: Dict[str, type], passed_values: Dict[str, Any]):
    res = {}
    for field_name in fields:
        if fields[field_name] == str:
            pass
        elif fields[field_name] == int:
            pass
        else:
            raise Exception('Unsupported factory type')
    return res
