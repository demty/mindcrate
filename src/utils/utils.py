import json
from datetime import date, datetime, time

from aiohttp import web


def dthandler(obj):
    """Сериализатор даты"""
    return obj.strftime("%Y-%m-%dT%H:%M:%S") if type(obj) in [datetime, date, time] else None


def custom_dump(*args, **kwargs):
    """Кастомный сериализатор, который умеет в datetime"""
    return json.dumps(*args, **kwargs, default=dthandler)


def json_response(data, status):
    """
    Ответ сервиса в виде json

    :param data: данные
    :type data: dict

    :param status: код ответа
    :type status: int
    """
    return web.json_response(data, status=status, dumps=custom_dump)
