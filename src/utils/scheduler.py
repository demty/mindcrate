import asyncio
import logging
from typing import Callable

from aiohttp.web import Application

logger = logging.getLogger(__name__)


async def scheduler(app: Application, task: Callable):
    """Запускалка бэкграунд-процессов с периодичностью"""
    try:
        while True:
            # Чтобы получить пул и сессию приложения
            try:
                await task(app=app)
            except Exception as e:
                logger.exception(e)
    except asyncio.CancelledError:
        pass
