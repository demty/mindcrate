from dataclasses import dataclass
from typing import Dict, Any, Optional


@dataclass
class CustomerInfo:
    phone: Optional[str]
    email: Optional[str]

    @classmethod
    def from_json(cls, data: Dict[str, Any]) -> 'CustomerInfo':
        mp = data.get('mobilePhone')
        if mp:
            mp = str(mp)
        return cls(
            phone=mp, email=data.get('email')
        )
