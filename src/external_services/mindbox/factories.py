from aiohttp.client import ClientSession
from external_services.mindbox.controller import MindboxController
from external_services.mindbox.entities import MindboxRequestParams
from external_services.mindbox.exceptions import MissingData
from external_services.mindbox.repository import MindboxRepository, MockRepository
from external_services.mindbox.service import MindboxService
from settings import MINDBOX_MODE_REPO


class ControllerFactory:
    """Фабрика контроллеров майндбокса"""

    @classmethod
    def get_controller(cls, session: ClientSession) -> MindboxController:
        """Создать контроллер"""
        if MINDBOX_MODE_REPO == 'production':
            repo = MindboxRepository(session=session)
        else:
            repo = MockRepository()
        service = MindboxService(repository=repo)
        cont = MindboxController(service=service)
        return cont


class MindboxRequestParamsFactory:
    """фабрика данных для запроса в майндбокс"""

    @classmethod
    def create_request_params(cls, device_uuid: str, customer_ip: str, user_agent: str) -> MindboxRequestParams:
        """Создать дто"""
        if not device_uuid or not customer_ip or not user_agent:
            raise MissingData('Не переданы необходимые параметры для запроса в майндбокс, нужно: deviceUuid, '
                              'customerIp, userAgent')
        return MindboxRequestParams(device_uuid=device_uuid, customer_ip=customer_ip, user_agent=user_agent)
