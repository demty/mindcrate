import pytest
from aiohttp import ClientSession
from pytest_mock import MockFixture

from external_services.mindbox.controller import NewMindboxController

BASE_URL = 'http://example.com'
SHORT_URL = 'example.com'


@pytest.fixture()
async def patch_mindbox_url(mocker: MockFixture):
    mocker.patch('settings.MINDBOX_ENDPOINT_EXTERNAL', BASE_URL)


@pytest.fixture()
async def mindbox_controller(patch_mindbox_url):
    session = ClientSession()
    c = NewMindboxController.from_params(session=session)
    yield c
    await session.close()
