GET_SEGMENT_USERS_ANSWER = dict(
    status=200,
    json={
        "status": "Success",
        "customers": [
            {
                "firstName": "Test",
                "email": "test@test.com",
                "isEmailInvalid": False,
                "mobilePhone": 79111111111,
                "isMobilePhoneInvalid": False,
                "sex": "female",
                "customFields": {
                    "sex": "female",
                    "userKey": "132"
                },
                "changeDateTimeUtc": "2019-01-01T09:03:27.743Z",
                "ianaTimeZone": "Asia/Yekaterinburg",
                "timeZoneSource": "Определили в трекере"
            }
        ]
    }
)
