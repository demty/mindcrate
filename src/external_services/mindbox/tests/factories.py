import factory

from external_services.mindbox.answer_entities import CustomerInfo


class CustomerInfoFactory(factory.Factory):
    class Meta:
        model = CustomerInfo

    phone = factory.Sequence(lambda n: '7%10d' % n)
    email = factory.Sequence(lambda n: 'test%s@test%s.com' % (n, n))
