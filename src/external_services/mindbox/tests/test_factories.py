import pytest

from external_services.mindbox.exceptions import MissingData
from external_services.mindbox.factories import MindboxRequestParamsFactory


class TestRequestParamsFactory:
    def test_all_params(self):
        r = MindboxRequestParamsFactory.create_request_params(
            device_uuid='123', customer_ip='1.1.1.1', user_agent='test'
        )
        assert r

    def test_param_missing(self):
        with pytest.raises(MissingData):
            MindboxRequestParamsFactory.create_request_params(
                device_uuid='123', customer_ip='1.1.1.1', user_agent=None
            )
