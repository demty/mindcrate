from aiohttp.web_response import json_response
from aresponses import ResponsesMockServer

import settings
from external_services.mindbox.controller import NewMindboxController
from external_services.mindbox.tests.answers import GET_SEGMENT_USERS_ANSWER
from external_services.mindbox.tests.fixtures import SHORT_URL

OPERATION_NAME = 'get_users_test'
START_PAGE = 0
OFFSET = 1


class TestMindboxController:
    class TestGetSegmentUsers:
        async def test_get_users_one_user(self, mindbox_controller: NewMindboxController,
                                          aresponses: ResponsesMockServer):
            aresponses.add(
                SHORT_URL,
                f'/v3/operations/sync?endpointId={settings.MINDBOX_DEFAULT_BRAND}&operation={OPERATION_NAME}',
                'POST', json_response(
                    data=GET_SEGMENT_USERS_ANSWER['json'], status=GET_SEGMENT_USERS_ANSWER['status']
                ), match_querystring=True
            )
            res = await mindbox_controller.load_customers_by_segment(operation_name=OPERATION_NAME,
                                                                     start_page=START_PAGE,
                                                                     offset=OFFSET)
            assert len(res) == 1
            # In mb phones stored as int
            assert res[0].phone == str(GET_SEGMENT_USERS_ANSWER['json']['customers'][0]['mobilePhone'])
