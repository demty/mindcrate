import logging
from typing import Dict, Any, Optional

from aiohttp.client import ClientSession

import settings
from external_services.mindbox.entities import MindboxFullRequest, MindboxOperationType

logger = logging.getLogger(__name__)


class MockRepository:
    """Репозиторий из моков, работает в деве и релизе"""

    def __getattribute__(self, *args, **kwargs):
        async def mock_f(*args, **kwargs):
            logger.debug(f'args: {args}, kwargs: {kwargs}')

        return mock_f


class NewMindboxRepository:
    def __init__(self, session: ClientSession):
        """Конструктор"""
        self._session = session

    async def perform_request(self, full_request_params: MindboxFullRequest) -> Optional[Dict[str, Any]]:
        return await self._build_and_perform_request(
            operation=full_request_params.operation, post_data=full_request_params.post_data,
            device_uuid=full_request_params.request_params.device_uuid, endpoint_id=full_request_params.endpointId,
            customer_ip=full_request_params.request_params.customer_ip, mb_token=full_request_params.secret_key,
            use_access_token=full_request_params.use_secret_key, op_type=full_request_params.operation_type,
            user_agent=full_request_params.request_params.user_agent,
            append_content_type=full_request_params.append_content_type
        )

    async def _build_and_perform_request(self, operation: str, post_data: Dict, device_uuid: str or None,
                                         customer_ip: str or None, user_agent: str or None,
                                         use_access_token: bool = False, mb_token: str = settings.MINDBOX_ACCESS_KEY,
                                         endpoint_id: str = settings.MINDBOX_DEFAULT_BRAND,
                                         op_type: MindboxOperationType = MindboxOperationType.async_op,
                                         append_content_type: bool = False) \
            -> Optional[Dict[str, Any]]:
        """Собрать и выполнить запрос в майндбокс"""
        token = None
        if use_access_token:
            token = mb_token
        headers = await self._get_base_headers(customer_ip=customer_ip, user_agent=user_agent, token=token)
        if append_content_type:
            headers['Accept'] = 'application/json'
            headers['Content-Type'] = 'application/json'
        params = dict(
            endpointId=endpoint_id,
            operation=operation
        )
        if device_uuid:
            params['deviceUUID'] = device_uuid
        return await self._perform_mindbox_request(
            headers=headers, post_data=post_data, operation=operation, device_uuid=device_uuid, params=params,
            op_type=op_type
        )

    async def _get_base_headers(self, customer_ip: str, user_agent: str, token: str) -> Dict:
        """Получить хедеры для запроса"""
        res = dict()
        if customer_ip:
            res['X-Customer-IP'] = customer_ip
        if user_agent:
            res['User-Agent'] = user_agent
        if token:
            res['Authorization'] = f'Mindbox secretKey="{token}"'
        return res

    async def _perform_mindbox_request(self, headers: Dict, post_data: Dict, operation: str, device_uuid: str,
                                       params: Dict, op_type: MindboxOperationType = MindboxOperationType.async_op) \
            -> Optional[Dict[str, Any]]:
        """Сделать запрос в майндбокс. Т.к. все запросы примерно одинаковые - сделал единый метод"""
        async with self._session.post(f'{settings.MINDBOX_ENDPOINT_EXTERNAL}/v3/operations/{op_type.value}',
                                      params=params,
                                      json=post_data,
                                      headers=headers) as resp:
            if resp.status != 200:
                logger.exception({'msg': f'Не удалось выполнить операцию {operation}',
                                  'data': {**post_data, 'uuid': device_uuid},
                                  'text': await resp.text()}
                                 )
                return None
            else:
                return await resp.json()


class MindboxRepository(NewMindboxRepository):
    """Репозиторий майндбокса, работает в проде"""

    async def setup_cart(self, post_data: Dict, device_uuid: str, customer_ip: str, user_agent: str):
        """Установить корзину"""
        await self._build_and_perform_request(
            operation='SetCartList', post_data=post_data, device_uuid=device_uuid, customer_ip=customer_ip,
            user_agent=user_agent
        )
