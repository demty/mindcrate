from typing import Any, Dict, List
from external_services.mindbox.presenter_mixins import ProductMixin
from internal_services.waitlist.entities import WaitlistItem


class WaitlistMixin(ProductMixin):
    @classmethod
    def get_waitlist_present(cls, products: List[WaitlistItem]) -> Dict[str, Any]:
        cards = []
        for x in products:
            try:
                card = x.get_product_card()
                item = card.get_item(x.get_product_item_id())
                cards.append({
                    'card': card,
                    'item': item
                })
            except AttributeError:
                pass
        return {
            'productList': [
                cls.get_product_present(product_card=x['card'], product_item=x['item']) for x in cards
            ]
        }

    @classmethod
    def clear_waitlist(cls):
        return {}
