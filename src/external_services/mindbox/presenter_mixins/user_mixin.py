from enum import Enum, auto
from typing import Any, Dict, Set

from lobster_client.entities import UserEntity


class UserForceEditFields(Enum):
    userKey = auto()


class UserMixin:
    @classmethod
    def get_simple_user_present(cls, user: UserEntity) -> Dict[str, Any]:
        return {
            'customer': {
                "mobilePhone": user.get_phone()
            }
        }

    @classmethod
    def get_simple_pe_user_present(cls, user: UserEntity) -> Dict[str, Any]:
        return {
            'customer': {
                "mobilePhone": user.get_phone(),
                'email': user.get_email()
            }
        }

    @classmethod
    def _form_subscriptions(cls, user: UserEntity, with_value_by_default: bool = False) -> Dict[str, Any]:
        base = {
            'subscriptions': [
                {
                    'brand': 'Ennergiia',
                    'pointOfContact': 'Email',
                    'isSubscribed': user.get_mindbox_subscription_status()
                },
                {
                    'brand': 'Ennergiia',
                    'pointOfContact': 'Sms',
                    'isSubscribed': user.get_mindbox_sms_subscription_status()
                },
            ]
        }
        if with_value_by_default:
            for sub in base['subscriptions']:
                sub['valueByDefault'] = True
        return base

    @classmethod
    def get_user_force_update_present(cls, user: UserEntity, fields: Set[UserForceEditFields]):
        data = cls.get_simple_user_present(user=user)
        for field in fields:
            if field == UserForceEditFields.userKey:
                data['customer']['customFields'] = {
                    **data.get('customFields', {}),
                    'userKey': user.get_entity_id()
                }
        return data

    @classmethod
    def get_user_present(cls, user: UserEntity, with_value_by_default: bool = False) -> Dict[str, Any]:
        return {
            'customer': {
                'sex': user.get_sex(),
                'email': user.get_email(),
                'customFields': {
                    'sex': user.get_sex(),
                    'userKey': user.get_entity_id()
                },
                'lastName': user.get_surname(),
                'firstName': user.get_first_name(),
                'birthDate': user.get_birth_date(),
                'mobilePhone': user.get_phone(),
                **cls._form_subscriptions(user=user, with_value_by_default=with_value_by_default)
            }
        }
