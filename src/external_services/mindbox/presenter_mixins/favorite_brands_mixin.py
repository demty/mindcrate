from typing import Any, Dict, List

from alligator_client.entities import BrandItem


class FavoriteBrandsMixin:
    @classmethod
    def get_favorite_brands_present(cls, favorite_brands: List[BrandItem]) -> Dict[str, Any]:
        return {
            'favouriteBrands': [
                x.name for x in favorite_brands
            ] if favorite_brands else ['0']
        }
