from alligator_client.entities import ProductCard, ProductCardItem
from typing import Any, Dict, Optional


class ProductMixin:
    @classmethod
    def get_product_present(cls, product_card: ProductCard, product_item: Optional[ProductCardItem]) -> Dict[str, Any]:
        res_d = cls._get_product_present(product_card)
        if product_item:
            res_d['product'] = {
                **res_d['product'],
                **cls._get_product_item_present(product_item, product_card.is_new, product_card.country_of_origin)
            }
        return res_d

    @classmethod
    def _get_product_item_present(cls, product_item: ProductCardItem, is_new: bool, country: str) -> Dict[str, Any]:
        return {
            'sku': {
                'ids': {
                    'online': product_item.id
                },
                'name': product_item.name,
                'url': product_item.url,
                'pictureUrl': product_item.get_image(0),
                'price': product_item.price,
                'customFields': {
                    'colorSKU': product_item.color,
                    'countryOfOrigin': country,
                    'isNew': is_new,
                    'sizeSKU': product_item.size
                }
            }
        }

    @classmethod
    def _get_product_present(cls, product_card: ProductCard) -> Dict[str, Any]:
        return {
            'product': {
                'ids': {
                    'online': product_card.id
                },
                'name': product_card.name,
                'url': product_card.url,
                'pictureUrl': product_card.get_image(0, from_first_item=True),
                'customFields': {
                    'isNewProduct': product_card.is_new,
                },
                'price': product_card.get_price(from_first_item=True)
            },
            'count': 1,
            'price': product_card.get_price(from_first_item=True)
        }
