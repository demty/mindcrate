from typing import Any, Dict, List
from external_services.mindbox.presenter_mixins import ProductMixin
from internal_services.wishlist.entities import WishlistItem


class WishlistMixin(ProductMixin):
    @classmethod
    def get_wishlist_present(cls, products: List[WishlistItem], unique_products_only: bool = True) -> Dict[str, Any]:
        if unique_products_only:
            unique_products_ids = []
            unique_products = []
            for product in products:
                if product.get_product_id() not in unique_products_ids:
                    unique_products.append(product)
                    unique_products_ids.append(product.get_product_id())
        else:
            unique_products = products
        return {
            'productList': [
                cls.get_product_present(product_card=x.get_product_card(), product_item=None) for x in unique_products
            ]
        }

    @classmethod
    def clear_wishlist(cls):
        return {}
