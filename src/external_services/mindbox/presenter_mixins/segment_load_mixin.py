import datetime


class SegmentLoadMixin:
    @classmethod
    def get_segment_load_page_present(cls, offset: int, start_page: int):
        dt_old = datetime.datetime(year=2016, month=12, day=1, tzinfo=datetime.timezone.utc)
        dt_now = datetime.datetime.utcnow()
        now_minus_10_min = dt_now - datetime.timedelta(minutes=10)
        return {
            'customersPage': {
                'pageNumber': start_page + 1,
                'itemsPerPage': offset,
                'sinceDateTimeUtc': dt_old.isoformat()[:-6] + 'Z',
                'tillDateTimeUtc': now_minus_10_min.isoformat()[:-6] + 'Z'
            }
        }
