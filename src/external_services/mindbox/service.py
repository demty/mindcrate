from typing import List, Dict, Any, Set, Optional

from alligator_client.entities import ProductCard, ProductCardItem, BrandItem
from lobster_client.entities import UserEntity

from external_services.mindbox.answer_entities import CustomerInfo
from external_services.mindbox.entities import MindboxFullRequest, MindboxRequestParams, RegisterParams, \
    MindboxOperationType
from external_services.mindbox.mindbox_presenter import MindboxPresenter as NewMindboxPresenter
from external_services.mindbox.presenter_mixins import UserForceEditFields
from external_services.mindbox.presenter import MindboxPresenter
from external_services.mindbox.repository import MindboxRepository, NewMindboxRepository
from internal_services.massmedia.entities import JobAttemptDto
from internal_services.order.entities import OrderDTO
from internal_services.waitlist.entities import WaitlistItem
from internal_services.wishlist.entities import WishlistItem
from sap_client.entities import ClientSapOrder


class NewMindboxService:

    def __init__(self, repository: NewMindboxRepository):
        """Конструткор"""
        self._repo = repository

    async def _call_repo(self, operation: str, post_data: Dict[str, Any], request_params: MindboxRequestParams,
                         use_secret_key: bool = False,
                         operation_type: MindboxOperationType = MindboxOperationType.async_op,
                         append_content_type: bool = False)\
            -> Optional[Dict[str, Any]]:
        r = MindboxFullRequest(
            request_params=request_params, operation=operation, post_data=post_data, use_secret_key=use_secret_key,
            operation_type=operation_type, append_content_type=append_content_type
        )
        return await self._repo.perform_request(
            full_request_params=r
        )

    async def update_wishlist(self, wishlist: List[WishlistItem], request_params: MindboxRequestParams):
        data = NewMindboxPresenter.get_wishlist_present(wishlist)
        await self._call_repo('mock', data, request_params)

    async def load_customers_by_segment(self, operation_name: str, start_page: int, offset: int)\
            -> Optional[List[CustomerInfo]]:
        data = NewMindboxPresenter.get_segment_load_page_present(offset=offset, start_page=start_page)
        res_data = await self._call_repo(
            operation=operation_name, post_data=data, use_secret_key=True,
            operation_type=MindboxOperationType.sync_op, append_content_type=True,
            request_params=MindboxRequestParams(customer_ip='127.0.0.1', user_agent='python-requests', device_uuid=None)
        )
        if res_data:
            return [CustomerInfo.from_json(x) for x in res_data.get('customers', [])]
