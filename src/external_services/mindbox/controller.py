import logging
from aiohttp import ClientSession
from typing import List, Set, Optional

import settings
from alligator_client.entities import ProductCard, ProductCardItem, BrandItem
from lobster_client.entities import UserEntity

from external_services.mindbox.answer_entities import CustomerInfo
from external_services.mindbox.entities import MindboxRequestParams, RegisterParams
from external_services.mindbox.exceptions import MissingData
from external_services.mindbox.presenter_mixins import UserForceEditFields
from external_services.mindbox.repository import MockRepository, NewMindboxRepository
from external_services.mindbox.service import MindboxService, NewMindboxService
from internal_services.massmedia.entities import JobAttemptDto
from internal_services.order.entities import OrderDTO
from internal_services.order.exceptions import EmptyOrder
from internal_services.waitlist.entities import WaitlistItem
from internal_services.waitlist.exceptions import ProductEmpty
from internal_services.wishlist.entities import WishlistItem
from sap_client.entities import ClientSapOrder

logger = logging.getLogger(__name__)


class NewMindboxController:
    def __init__(self, service: NewMindboxService):
        self._service = service

    @classmethod
    def from_params(cls, session: ClientSession) -> 'NewMindboxController':
        if settings.MINDBOX_MODE_REPO != 'production':
            repo = MockRepository()
        else:
            repo = NewMindboxRepository(session=session)
        s = NewMindboxService(repository=repo)
        return cls(service=s)

    async def update_wishlist(self, wishlist: List[WishlistItem], request_params: MindboxRequestParams):
        if wishlist:
            await self._service.update_wishlist(wishlist=wishlist, request_params=request_params)
        else:
            await self._service.clear_wishlist(request_params=request_params)

    async def load_customers_by_segment(self, operation_name: str, start_page: int, offset: int)\
            -> Optional[List[CustomerInfo]]:
        # NB: mindbox page starts from 1, but you can pass 0, presenter adds 1 to page value
        return await self._service.load_customers_by_segment(
            operation_name=operation_name, start_page=start_page, offset=offset
        )

    def _is_waitlist_ok(self, waitlist: List[WaitlistItem]) -> bool:
        try:
            for item in waitlist:
                item.get_product_card()
            return True
        except ProductEmpty:
            return False
