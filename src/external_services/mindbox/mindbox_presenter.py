from typing import List, Any, Dict

from lobster_client.entities import UserEntity
from external_services.mindbox.presenter_mixins import (
    WaitlistMixin, WishlistMixin, UserMixin, SegmentLoadMixin
)
from internal_services.waitlist.entities import WaitlistItem
from internal_services.wishlist.entities import WishlistItem


class MindboxPresenter(WaitlistMixin, WishlistMixin, UserMixin, SegmentLoadMixin):
    @classmethod
    def get_wishlist_present_by_phone(cls, user: UserEntity, wishlist: List[WishlistItem]) -> Dict[str, Any]:
        cust = cls.get_simple_user_present(user)
        wl = cls.get_wishlist_present(wishlist)
        return {
            **cust,
            **wl
        }

    @classmethod
    def get_waitlist_present_by_phone(cls, user: UserEntity, waitlist: List[WaitlistItem]) -> Dict[str, Any]:
        cust = cls.get_simple_user_present(user)
        wl = cls.get_waitlist_present(waitlist)
        return {
            **cust,
            **wl
        }

    @classmethod
    def clear_waitlist_by_phone(cls, user: UserEntity):
        cust = cls.get_simple_user_present(user)
        wl = cls.clear_waitlist()
        return {
            **wl,
            **cust
        }

    @classmethod
    def clear_wishlist_by_phone(cls, user: UserEntity):
        cust = cls.get_simple_user_present(user)
        wl = cls.clear_wishlist()
        return {
            **wl,
            **cust
        }
