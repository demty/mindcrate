from enum import Enum

import settings
from dataclasses import dataclass
from typing import Any, Optional


class MindboxRequestParams:
    """Параметры для запроса в майндбокс"""

    def __init__(self, user_agent: Optional[str], customer_ip: Optional[str], device_uuid: Optional[str]):
        """Конструктор"""
        self.user_agent = user_agent
        self.customer_ip = customer_ip
        self.device_uuid = device_uuid

    def __repr__(self):
        return str({'user_agent': self.user_agent, 'customer_ip': self.customer_ip, 'device_uuid': self.device_uuid})


class MindboxOperationType(Enum):
    sync_op = 'sync'
    async_op = 'async'


@dataclass
class MindboxFullRequest:
    request_params: MindboxRequestParams
    post_data: Any
    operation: str
    use_secret_key: bool = False
    operation_type: MindboxOperationType = MindboxOperationType.async_op
    endpointId: str = settings.MINDBOX_DEFAULT_BRAND
    secret_key: str = settings.MINDBOX_ACCESS_KEY
    append_content_type: bool = False

    def __repr__(self):
        return str({'data': self.post_data, 'request_params': self.request_params, 'op': self.operation,
                    'use_secret_key': self.use_secret_key, 'secret_key': self.secret_key,
                    'endpoint': self.endpointId})


class RegisterParams:
    """Параметры регистрации пользователя"""

    def __init__(self, email: str, brand: str):
        """Конструктор"""
        self.email = email
        self.brand = brand

    @classmethod
    def from_params(cls, email: str, brand: str) -> 'RegisterParams':
        """Фабрика сущности"""
        return cls(email=email, brand=brand if brand else settings.MINDBOX_DEFAULT_BRAND)
