class WrongApiAnswer(Exception):
    pass


class TokenExpiredOrNotSet(WrongApiAnswer):
    pass
