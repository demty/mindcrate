from dataclasses import dataclass
from enum import Enum
from typing import Optional, Any, Dict, List


@dataclass
class Audience:
    id: Optional[int]
    advertiser_id: int
    name: str
    description: Optional[str]

    @classmethod
    def from_json(cls, data: Dict[str, Any]) -> 'Audience':
        return cls(
            id=data['id'], advertiser_id=data['advertiserId'], name=data['name'], description=data['description']
        )

    @classmethod
    def from_create_method(cls, data: Dict[str, Any], advertiser_id: int, name: str, description: str) -> 'Audience':
        return cls(
            id=data['audienceId'], name=name, description=description, advertiser_id=advertiser_id
        )


@dataclass
class AudienceUsersActionResult:
    operation: str
    request_date: str
    schema: str
    valid_emails: int
    invalid_emails: int
    sample_invalid_emails: Optional[List[str]]

    @classmethod
    def from_json(cls, data: Dict[str, Any]) -> 'AudienceUsersActionResult':
        return cls(operation=data['operation'], request_date=data['requestDate'], schema=data['schema'],
                   valid_emails=data['nbValidIdentifiers'], invalid_emails=data['nbInvalidIdentifiers'],
                   sample_invalid_emails=data['sampleInvalidIdentifiers'])


class AudienceUsersOperation(Enum):
    add = 'add'
    remove = 'remove'
