TOKEN_ANSWER = dict(
    status=200,
    json={
        "access_token": "some_token",
        "expires_in": 0,
        "token_type": "bearer"
    }
)

AUDIENCES_ANSWER = dict(
    status=200,
    json={
        "audiences": [
            {
                "id": 1,
                "advertiserId": 0,
                "name": "test",
                "description": "test",
                "created": 0,
                "updated": 0,
                "nbLines": 0,
                "nbLinesEmail": 0,
                "nbMatchesEmail": 0
            },
            {
                "id": 2,
                "advertiserId": 0,
                "name": "test2",
                "description": "test2",
                "created": 0,
                "updated": 0,
                "nbLines": 0,
                "nbLinesEmail": 0,
                "nbMatchesEmail": 0
            }
        ]
    }
)

AUDIENCE_CREATE_ANSWER = dict(
    status=200,
    json={
        "audienceId": 123
    }
)

ADD_USERS_TO_AUDIENCE_ANSWER = dict(
    status=200,
    json={
        "operation": "add",
        "requestDate": "2019-08-11T19:45:02.536Z",
        "schema": "email",
        "nbValidIdentifiers": 1,
        "nbInvalidIdentifiers": 1,
        "sampleInvalidIdentifiers": [
            'some_email.error'
        ]
    }
)
