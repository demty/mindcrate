import pytest
from aiohttp.web_response import json_response

from aresponses.main import ResponsesMockServer

from external_services.criteo import CriteoClient
from external_services.criteo.entities import AudienceUsersOperation, AudienceUsersActionResult
from external_services.criteo.exceptions import TokenExpiredOrNotSet
from external_services.criteo.tests.answers import AUDIENCES_ANSWER, AUDIENCE_CREATE_ANSWER, \
    ADD_USERS_TO_AUDIENCE_ANSWER, TOKEN_ANSWER
from external_services.criteo.tests.factories import AudienceFactory
from external_services.criteo.tests.fixtures import SHORT_URL


class TestReissueToken:
    async def test_token_retrieved(self, criteo_client: CriteoClient, aresponses: ResponsesMockServer):
        aresponses.add(
            SHORT_URL, '/oauth2/token', 'POST', json_response(
                data=TOKEN_ANSWER['json'], status=TOKEN_ANSWER['status']
            )
        )
        await criteo_client.reissue_token()
        assert criteo_client._token == TOKEN_ANSWER['json']['access_token']

    async def test_error_while_token_retrieve(self, criteo_client: CriteoClient, aresponses: ResponsesMockServer):
        aresponses.add(
            SHORT_URL, '/oauth2/token', 'POST', json_response(
                data={'oops': 'some error'}, status=401
            )
        )
        with pytest.raises(TokenExpiredOrNotSet):
            await criteo_client.reissue_token()
        assert criteo_client._token is None


class TestGetAudiences:
    async def test_two_audiences(self, criteo_client_with_token: CriteoClient, aresponses: ResponsesMockServer):
        aresponses.add(
            SHORT_URL, '/v1/audiences', 'GET', json_response(
                data=AUDIENCES_ANSWER['json'], status=AUDIENCES_ANSWER['status']
            )
        )
        auds = await criteo_client_with_token.get_audiences()
        assert len(auds) == 2


class TestCreateAudience:
    async def test(self, criteo_client_with_token: CriteoClient, aresponses: ResponsesMockServer):
        aud = AudienceFactory(id=None)
        aresponses.add(
            SHORT_URL, '/v1/audiences/userlist', 'POST', json_response(
                data=AUDIENCE_CREATE_ANSWER['json'], status=AUDIENCE_CREATE_ANSWER['status']
            )
        )
        res_aud = await criteo_client_with_token.create_audience(aud.name, aud.description)
        assert res_aud.name == aud.name
        assert res_aud.description == aud.description
        assert res_aud.id == AUDIENCE_CREATE_ANSWER['json']['audienceId']


class TestClearAudience:
    async def test(self, criteo_client_with_token: CriteoClient, aresponses: ResponsesMockServer):
        aud = AudienceFactory()
        aresponses.add(
            SHORT_URL, f'/v1/audiences/userlist/{aud.id}/users', 'DELETE', json_response(
                data={'some_answer': ''}, status=200
            )
        )
        await criteo_client_with_token.clear_audience(aud)


class TestDeleteAudience:
    async def test(self, criteo_client_with_token: CriteoClient, aresponses: ResponsesMockServer):
        aud = AudienceFactory()
        aresponses.add(
            SHORT_URL, f'/v1/audiences/{aud.id}', 'DELETE', json_response(
                data={'some_answer': ''}, status=200
            )
        )
        await criteo_client_with_token.delete_audience(aud)


class TestAddToAudience:
    async def test(self, criteo_client_with_token: CriteoClient, aresponses: ResponsesMockServer):
        aud = AudienceFactory()
        aresponses.add(
            SHORT_URL, f'/v1/audiences/userlist/{aud.id}', 'PATCH', json_response(
                data=ADD_USERS_TO_AUDIENCE_ANSWER['json'], status=ADD_USERS_TO_AUDIENCE_ANSWER['status']
            )
        )
        res = await criteo_client_with_token.add_or_remove_users_to_audience(
            operation=AudienceUsersOperation.add, audience=aud, users_emails=['test@test.com', 'somebad.email']
        )
        assert type(res) == AudienceUsersActionResult
        assert res.operation == 'add'
        assert res.valid_emails == 1
        assert res.invalid_emails == 1
        assert len(res.sample_invalid_emails) == 1
