import pytest
from aiohttp import ClientSession

from external_services.criteo import CriteoClient

BASE_URL = 'http://example.com'
SHORT_URL = 'example.com'
CLIENT_ID = '1'
CLIENT_SECRET = '2'
ADVERTISED_ID = 3


@pytest.fixture()
async def criteo_client():
    c = CriteoClient(
        base_url=BASE_URL, client_id=CLIENT_ID, client_secret=CLIENT_SECRET, advertiser_id=ADVERTISED_ID,
        session=ClientSession()
    )
    yield c
    await c._session.close()


@pytest.fixture()
async def criteo_client_with_token(criteo_client: CriteoClient):
    criteo_client._token = 'some_token'
    yield criteo_client
