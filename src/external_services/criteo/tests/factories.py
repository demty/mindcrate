import factory

from external_services.criteo.entities import Audience


class AudienceFactory(factory.Factory):
    class Meta:
        model = Audience

    name = factory.Sequence(lambda n: 'name%s' % n)
    description = factory.Sequence(lambda n: 'desc%s' % n)
    id = factory.Sequence(lambda n: n)
    advertiser_id = 123

