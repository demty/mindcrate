import logging
from typing import Any, Dict, List
from urllib.parse import urljoin

from aiohttp import ClientSession

from external_services.criteo.entities import Audience, AudienceUsersOperation, AudienceUsersActionResult
from external_services.criteo.exceptions import TokenExpiredOrNotSet, WrongApiAnswer

logger = logging.getLogger(__name__)


class CriteoClient:
    def __init__(self, base_url: str, client_id: str, client_secret: str, advertiser_id: int, session: ClientSession):
        self._token = None
        self._client_id = client_id
        self._client_secret = client_secret
        self._advertiser_id = advertiser_id
        self._base_url = base_url
        self._session = session

    async def _check_token(self):
        if not self._token:
            await self.reissue_token()

    async def get_audiences(self) -> List[Audience]:
        url = urljoin(self._base_url, 'v1/audiences')
        params = {
            'advertiserId': self._advertiser_id
        }
        data = await self._call_with_retry_for_token(url, params=params)
        auds = []
        for audience_json in data['audiences']:
            auds.append(Audience.from_json(audience_json))
        return auds

    async def _call_with_retry_for_token(self, *args, **kwargs) -> Dict:
        try:
            data = await self._make_request(*args, **kwargs)
        except TokenExpiredOrNotSet:
            await self.reissue_token()
            data = await self._make_request(*args, **kwargs)
        return data

    async def create_audience(self, name: str, description: str) -> Audience:
        url = urljoin(self._base_url, 'v1/audiences/userlist')
        json = {
            'name': name,
            'description': description,
            'advertiserId': self._advertiser_id
        }
        data = await self._call_with_retry_for_token(url, json=json, method='POST')
        return Audience.from_create_method(data=data, name=name.lower(), description=description,
                                           advertiser_id=self._advertiser_id)

    async def clear_audience(self, audience: Audience):
        url = urljoin(self._base_url, f'v1/audiences/userlist/{audience.id}/users')
        await self._call_with_retry_for_token(url, method='DELETE')

    async def delete_audience(self, audience: Audience):
        url = urljoin(self._base_url, f'v1/audiences/{audience.id}')
        await self._call_with_retry_for_token(url, method='DELETE')

    async def add_or_remove_users_to_audience(self, operation: AudienceUsersOperation, audience: Audience,
                                              users_emails: List[str]) -> AudienceUsersActionResult:
        json = {
            'operation': operation.value,
            'schema': 'email',
            'identifiers': users_emails
        }
        url = urljoin(self._base_url, f'v1/audiences/userlist/{audience.id}')
        data = await self._call_with_retry_for_token(url, json=json, method='PATCH')
        return AudienceUsersActionResult.from_json(data)

    async def reissue_token(self):
        data = await self._make_request(method='POST', data={
            'client_id': self._client_id, 'client_secret': self._client_secret, 'grant_type': 'client_credentials'
        }, url=urljoin(self._base_url, 'oauth2/token'), required_token=False)
        self._token = data['access_token']

    async def _make_request(self, url: str, method: str = 'GET', params: Dict[str, Any] = None,
                            headers: Dict[str, Any] = None, json: Dict[str, Any] = None, data: Dict[str, Any] = None,
                            required_token: bool = True):
        all_headers = {}
        if required_token:
            if self._token:
                all_headers['Authorization'] = f'Bearer {self._token}'
            else:
                await self.reissue_token()
        if headers:
            all_headers = headers
        async with self._session.request(method, url,
                                         params=params,
                                         headers=all_headers,
                                         data=data,
                                         json=json) as resp:
            if resp.status == 401:
                raise TokenExpiredOrNotSet('Auth error')
            elif resp.status != 200:
                txt = await resp.text()
                logger.exception({
                    'msg': f'Не удалось выполнить запрос к ртб {url} {params}',
                    'text': txt
                })
                raise WrongApiAnswer(txt)
            else:
                return await resp.json()
