TERMINAL_STATES = ['ONCLIENT', 'RETURNED', 'CANCELORD', 'RETURNING']

STATUSES = dict(
    approved='approved',
    pending='pending',
    declined='declined',
    to_send='to_send',
)
TERMINAL_STATES_DICT = dict(
    onclient='ONCLIENT',
    returned='RETURNED',
    canceled='CANCELORD',
    returning='RETURNING'
)
