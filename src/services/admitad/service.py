from logging import getLogger
from typing import List

from services.admitad.constants import STATUSES
from services.admitad.entities import TrackedOrder, TrackedOrderFactory
from services.admitad.exceptions import OrderNotTracked
from services.admitad.repository import AdmitadInternalRepository, AdmitadRepository, AdmitadRepositoryMock

logger = getLogger('admitad.service')


class AdmitadService:
    """Сервис адмитада"""

    def __init__(self, repository: AdmitadRepository or AdmitadRepositoryMock,
                 internal_repository: AdmitadInternalRepository):
        """Конструктор"""
        self._repo = repository
        self._db_repo = internal_repository

    async def postback_order(self, order_id: str, status: str, amount: float, reward: float, comment: str):
        """Отследить заказ"""
        await self._repo.postback_order(
            order_id=order_id, status=status, amount=amount, reward=reward, comment=comment
        )

    async def generate_track_entity(self, status: str, order_number: str, order_guid: str):
        """Создать сущность отслеживания заказа"""
        return TrackedOrderFactory.get_entity(order_number=order_number, status=status, status_update=None,
                                              order_guid=order_guid, submit=False)

    async def create_or_update_track(self, track: TrackedOrder):
        """Обновить или создать сущность отслеживания заказа в базе данных"""
        await self._db_repo.create_or_update_track(order_number=track.get_order_number(), status=track.get_status(),
                                                   update_date=track.get_status_update_dt(),
                                                   order_guid=track.get_order_guid(), submit=track.get_submit())

    async def get_track(self, order_number: str) -> TrackedOrder:
        """Получить сущность отслеживания заказа"""
        record = await self._db_repo.get_order_status(order_number=order_number)
        if not record:
            raise OrderNotTracked(f'Заказ {order_number} не был ранее отслеживаем')
        return TrackedOrderFactory.get_entity_from_record(record=record)

    async def get_unsubmitted_tracks(self, statuses=(STATUSES['pending'],)) -> List[TrackedOrder]:
        """Получить неотправленные треки"""
        records = await self._db_repo.get_not_submitted_tracks(
            statuses=statuses
        )
        if not records:
            logger.info('Не найдено неотправленных треков')
            return []
        return [TrackedOrderFactory.get_entity_from_record(record=record) for record in records]
