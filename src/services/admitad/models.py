import sqlalchemy as sa

meta = sa.MetaData()


admitad_tracks = sa.Table(
    'admitad_tracks', meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('order_number', sa.String(length=50), unique=True, nullable=False),
    sa.Column('order_guid', sa.String(length=50), unique=True, nullable=False),
    sa.Column('status', sa.String(length=50), nullable=False),
    sa.Column('status_updated_at', sa.DateTime, nullable=False),
    sa.Column('submit', sa.Boolean, nullable=False, default=True)
)
