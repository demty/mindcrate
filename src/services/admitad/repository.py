import datetime
import logging
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import insert
from typing import List

import settings
from aiohttp.client import ClientSession
from asyncpg import Record
from asyncpg.pool import Pool
from services.admitad.constants import STATUSES
from services.admitad.models import admitad_tracks

logger = logging.getLogger(__name__)


class AdmitadRepositoryMock:
    """Мок репозитория адмитада"""

    def __init__(self, session: ClientSession):
        """Конструктор"""
        self._session = session

    async def postback_order(self, order_id: str, status: str, amount: float, reward: float, comment: str):
        """Отследить заказ"""
        logger.debug({'order_id': order_id, 'status': status, 'amount': amount, 'reward': reward, 'comment': comment})


class AdmitadRepository(AdmitadRepositoryMock):
    """Репозиторий адмитада"""

    async def postback_order(self, order_id: str, status: str, amount: float, reward: float, comment: str):
        """Отсделить заказ"""
        data = dict(
            campaign_code=settings.ADMITAD_CAMPAIGN_CODE,
            revision_key=settings.ADMITAD_REVISION_KEY,
            order_id=str(order_id),
            status=status,
            amount=str(amount),
            # reward=str(reward),
            comment=comment
        )
        logger.debug(dict(
            order_id=str(order_id),
            status=status,
            amount=str(amount),
            # reward=str(reward),
            comment=comment
        ))
        async with self._session.get(
                f'{settings.ADMITAD_ENDPOINT_EXTERNAL}/rp',
                params=data
        ) as resp:
            resp_text = await resp.text()
            logger.debug(resp_text)
            if resp.status != 200:
                if resp.status != 200:
                    logger.exception(
                        {
                            'msg': f'Не удалось выполнить postback запрос',
                            'data': {
                                **data,
                                'text': await resp.text()
                            }
                        }
                    )


class MockRepository:
    """Мок репозитория"""

    async def create_or_update_track(self, order_number: str, status: str, update_date: datetime.datetime,
                                     submit: bool, order_guid: str):
        """Создать или обновить отслеживание заказа"""
        logger.debug(dict(order_number=order_number, status=status, status_update_dt=update_date, submit=submit,
                          order_guid=order_guid))

    async def get_order_status(self, order_number: str) -> Record or None:
        """Получить статус заказа"""
        logger.debug(dict(order_number=order_number))

    async def get_not_submitted_tracks(self, statuses=(STATUSES['pending'],)) -> List[Record]:
        """Получить неотправленные треки и отправить снова"""
        logger.debug(list(dict(statuses=statuses)))
        return []


class AdmitadInternalRepository(MockRepository):
    """Репозиторий для внутреннего отслеживания заказов"""

    def __init__(self, pool: Pool):
        """Конструктор"""
        self.pool = pool

    async def get_order_status(self, order_number: str) -> Record or None:
        """Получить сохраненный статус заказа"""
        sql = sa.select([admitad_tracks]).where(admitad_tracks.c.order_number == order_number)
        return await self.pool.fetchrow(
            sql
        )

    async def create_or_update_track(self, order_number: str, status: str, update_date: datetime.datetime,
                                     submit: bool, order_guid: str):
        """Создать или обновить отслеживание заказа"""
        upsert = insert(admitad_tracks).values(
            order_number=order_number, status=status,
            status_updated_at=update_date, submit=submit, order_guid=order_guid
        ).on_conflict_do_update(
            constraint='admitad_tracks_order_number_key',
            set_=dict(
                status=status, submit=submit, status_updated_at=update_date
            )
        )
        await self.pool.execute(upsert)

    async def get_not_submitted_tracks(self, statuses=(STATUSES['pending'],)) -> List[Record]:
        """Получить неотправленные треки и отправить снова"""
        sql = sa.select([admitad_tracks]).where(
            (admitad_tracks.c.status.in_(statuses)) & (admitad_tracks.c.submit.is_(False))
        )
        return await self.pool.fetch(sql)
