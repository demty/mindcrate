import datetime
from typing import Dict

from asyncpg import Record


class TrackedOrder:
    """Отслеженный заказ"""

    def __init__(self,
                 order_number: str,
                 status: str,
                 status_update: datetime.datetime or None,
                 submit: bool,
                 order_guid: str):
        """Конструктор"""
        self._order_number = order_number
        self._order_guid = order_guid
        self._status = status
        self._submit = submit
        self._status_update_dt = status_update if status_update else datetime.datetime.now()

    def get_order_number(self) -> str:
        """Получить номер заказа"""
        return self._order_number

    def get_status(self) -> str:
        """Получить статус заказа"""
        return self._status

    def get_status_update_dt(self) -> datetime.datetime:
        """Получить время последнего обновления статуса"""
        return self._status_update_dt

    def update_status_update_dt(self):
        """Обновить время обнволения статуса"""
        self._status_update_dt = datetime.datetime.now()

    def set_status(self, new_status: str):
        """Установить статус"""
        self._status = new_status
        self._status_update_dt = datetime.datetime.now()

    def set_submit(self, value: bool):
        """Установить статус отправки в адмитад"""
        self._submit = value

    def get_submit(self) -> bool:
        """Была ли запись отправлена в адмитад"""
        return self._submit

    def get_order_guid(self) -> str:
        """Получить гуид заказа"""
        return self._order_guid


class TrackedOrderFactory:
    """Фабрика отслеживания заказов"""

    @classmethod
    def get_entity_from_dict(cls, data: Dict) -> TrackedOrder:
        """Получить сущность из словаря"""
        return TrackedOrder(
            order_number=data.get('order_number'),
            status=data.get('status'),
            status_update=data.get('status_updated_at'),
            submit=data.get('submit'),
            order_guid=data.get('order_guid')
        )

    @classmethod
    def get_entity_from_record(cls, record: Record) -> TrackedOrder:
        """Сущность из записи бд"""
        return cls.get_entity_from_dict(
            data=dict(record)
        )

    @classmethod
    def get_entity(cls, order_number: str, status: str, status_update: datetime.datetime or None,
                   submit: bool, order_guid: str) -> TrackedOrder:
        """Получить сущность из параметров"""
        return TrackedOrder(
            order_number=order_number,
            status=status,
            status_update=status_update,
            submit=submit,
            order_guid=order_guid
        )
