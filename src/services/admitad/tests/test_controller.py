import pytest
import datetime
import settings
from services.admitad.constants import TERMINAL_STATES_DICT, STATUSES
from services.admitad.controller import AdmitadController
from services.admitad.controller_factory import ControllerFactory
from services.admitad.entities import TrackedOrder
from services.admitad.repository import AdmitadInternalRepository, AdmitadRepositoryMock
from services.admitad.service import AdmitadService
from services.admitad.tests.test_repository import clear_table
from internal_services.order.entities import OrderDTO
from sap_client.entities import ClientSapOrder
from mock import patch


@pytest.fixture(scope='function')
def controller(app):
    yield ControllerFactory.get_controller(session=app.session, pool=app.pool)


@pytest.fixture(scope='function')
def service(app):
    yield AdmitadService(repository=AdmitadRepositoryMock(session=app.session),
                         internal_repository=AdmitadInternalRepository(pool=app.pool))


@pytest.fixture(scope='function')
def mock_repository_for_unsubmitted_tracks():
    async def get_data(statuses=[]):
        if statuses == [STATUSES['pending']]:
            return [{'order_number': '1', 'order_guid': '1'}]
        elif statuses == [STATUSES['pending'], STATUSES['approved']]:
            return [{'order_number': '2', 'order_guid': '2'}, {'order_number': '3', 'order_guid': '3'}]
        else:
            return None

    patch1 = patch('services.admitad.repository.AdmitadInternalRepository.get_not_submitted_tracks',
                   side_effect=get_data)
    patch1.start()
    yield None
    patch1.stop()


@pytest.fixture(scope='function')
def mock_service_postback():
    async def mock(*args, **kwargs):
        return None
    patch1 = patch('services.admitad.service.AdmitadService.postback_order', side_effect=mock)
    patch1.start()
    yield None
    patch1.stop()


@pytest.fixture(scope='function')
def mock_controller_postback():
    async def mock(*args, **kwargs):
        return None
    patch1 = patch('services.admitad.controller.AdmitadController.postback_order', side_effect=mock)
    patch1.start()
    yield None
    patch1.stop()


@pytest.fixture(scope='function')
def mock_service_cout():
    async def mock(*args, **kwargs):
        return None
    patch1 = patch('services.admitad.controller.AdmitadService.create_or_update_track', side_effect=mock)
    patch1.start()
    yield None
    patch1.stop()


@pytest.fixture(scope='function')
def mock_service_gt():
    async def mock(order_number):
        if order_number == '1':
            return TrackedOrder(order_number='1', status=STATUSES['pending'], submit=False,
                                status_update=datetime.datetime.now(), order_guid='1')
        elif order_number == '2':
            return TrackedOrder(order_number='2', status=STATUSES['pending'], submit=True,
                                status_update=datetime.datetime.now(), order_guid='2')
        elif order_number == '3':
            return TrackedOrder(order_number='3', status=STATUSES['pending'], submit=True,
                                status_update=datetime.datetime.now(), order_guid='3')
        else:
            return None

        return None
    patch1 = patch('services.admitad.service.AdmitadService.get_track', side_effect=mock)
    patch1.start()
    yield None
    patch1.stop()


@pytest.mark.usefixtures('app_client', 'app_server')
class TestCheckIfOrderInTerminalState:
    async def test_state_one(self, controller: AdmitadController):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['onclient']
                )
            ]
        ))
        assert await controller.check_if_order_in_terminal_stage(sap_order=order)

    async def test_state_two(self, controller: AdmitadController):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['returned']
                )
            ]
        ))
        assert await controller.check_if_order_in_terminal_stage(sap_order=order)

    async def test_wrong_state(self, controller: AdmitadController):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='some wrong state'
                )
            ]
        ))
        assert not await controller.check_if_order_in_terminal_stage(sap_order=order)


@pytest.mark.usefixtures('app_client', 'app_server')
class TestUnsubmittedTracks:

    async def test_wrong_state(self, controller: AdmitadController, mock_repository_for_unsubmitted_tracks):
        res = await controller.get_unsubmitted_tracks(statuses=['test1'])
        assert not res

    async def test_one_state(self, controller: AdmitadController, mock_repository_for_unsubmitted_tracks):
        res = await controller.get_unsubmitted_tracks(statuses=[STATUSES['pending']])
        assert len(res) == 1
        assert res[0].get_order_number() == '1'

    async def test_two_states(self, controller: AdmitadController, mock_repository_for_unsubmitted_tracks):
        res = await controller.get_unsubmitted_tracks(statuses=[STATUSES['pending'], STATUSES['approved']])
        assert len(res) == 2
        assert res[0].get_order_number() == '2'
        assert res[1].get_order_number() == '3'

    async def test_no_tracks(self, controller: AdmitadController, mock_repository_for_unsubmitted_tracks):
        res = await controller.get_unsubmitted_tracks(statuses=[STATUSES['declined']])
        assert not res


@pytest.mark.usefixtures('app_client', 'app_server')
class TestPostbackOrder:

    async def test_pending(self, controller: AdmitadController, mock_service_postback):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='pending',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1'
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100,
                        sale=100
                    )
                )
            ]
        ))
        await controller.postback_order(sap_order=order, service_order=service_order)
        assert AdmitadService.postback_order.called
        AdmitadService.postback_order.assert_called_with(order_id='1', status=STATUSES['pending'], reward=0.1,
                                                         comment='В процессе доставки', amount=100)

    async def test_fully_on_client(self, controller: AdmitadController, mock_service_postback):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['onclient'],
                    PRODUCT_CODE='3'
                ),
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['onclient'],
                    PRODUCT_CODE='4'
                )
            ],
            NUMBER='3'
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=300
            ),
            positions=[
                dict(
                    productItemId='3',
                    price=dict(
                        base=150,
                        sale=150
                    )
                ),
                dict(
                    productItemId='4',
                    price=dict(
                        base=150,
                        sale=150
                    )
                )
            ]
        ))
        await controller.postback_order(sap_order=order, service_order=service_order)
        assert AdmitadService.postback_order.called
        AdmitadService.postback_order.assert_called_with(order_id='3', status=STATUSES['approved'], reward=0.1,
                                                         comment='Полность выкуплен', amount=300)

    async def test_partly_on_client(self, controller: AdmitadController, mock_service_postback):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['onclient'],
                    PRODUCT_CODE='5'
                ),
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['returned'],
                    PRODUCT_CODE='6'
                )
            ],
            NUMBER='4'
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=300
            ),
            positions=[
                dict(
                    productItemId='5',
                    price=dict(
                        base=150,
                        sale=150
                    )
                ),
                dict(
                    productItemId='6',
                    price=dict(
                        base=150,
                        sale=150
                    )
                )
            ]
        ))
        await controller.postback_order(sap_order=order, service_order=service_order)
        assert AdmitadService.postback_order.called
        AdmitadService.postback_order.assert_called_with(order_id='4', status=STATUSES['approved'], reward=0.1,
                                                         comment='Частично выкуплено', amount=150)

    async def test_terminal(self, controller: AdmitadController, mock_service_postback):
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS=TERMINAL_STATES_DICT['returned'],
                    PRODUCT_CODE='2'
                )
            ],
            NUMBER='2'
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=200
            ),
            positions=[
                dict(
                    productItemId='2',
                    price=dict(
                        base=200
                    )
                )
            ]
        ))
        await controller.postback_order(sap_order=order, service_order=service_order)
        assert AdmitadService.postback_order.called
        AdmitadService.postback_order.assert_called_with(order_id='2', status=STATUSES['declined'], reward=0.1,
                                                         comment='Полный возврат', amount=0)


@pytest.mark.usefixtures('app_client', 'app_server')
class TestStoreTrackOrder:
    async def test_saving(self, controller: AdmitadController):
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='pending',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        await controller.store_track_order(sap_order=order)
        track = (await controller.get_unsubmitted_tracks())[0]
        assert track.get_order_guid() == guid


@pytest.mark.usefixtures('app_client', 'app_server')
class TestTrackOrder:
    async def test_mark_to_send(self, controller: AdmitadController, service: AdmitadService, clear_table):
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        track = TrackedOrder(order_number='1', status=STATUSES['pending'], submit=False,
                             order_guid=guid, status_update=None)
        await service.create_or_update_track(track=track)
        sap_order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='ONCLIENT',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100
                    )
                )
            ]
        ))
        await controller.track_order(tracked_order=track, sap_order=sap_order, service_order=service_order)
        tested_track = await service.get_track(order_number='1')
        assert not tested_track.get_submit()
        assert tested_track.get_status() == STATUSES['to_send']

    async def test_dont_mark_to_send(self, controller: AdmitadController, service: AdmitadService, clear_table):
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        track = TrackedOrder(order_number='1', status=STATUSES['pending'], submit=False,
                             order_guid=guid, status_update=None)
        await service.create_or_update_track(track=track)
        sap_order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='ANOTHER_STATUS',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100
                    )
                )
            ]
        ))
        await controller.track_order(tracked_order=track, sap_order=sap_order, service_order=service_order)
        tested_track = await service.get_track(order_number='1')
        assert not tested_track.get_submit()
        assert tested_track.get_status() == STATUSES['pending']

    async def test_dont_send_order_because_of_status(self, controller: AdmitadController,
                                                     service: AdmitadService,
                                                     clear_table, mock_service_postback):
        init_dt = datetime.datetime.now() - datetime.timedelta(days=settings.ADMITAD_TERMINAL_WAIT_TIME_DAYS + 2)
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        track = TrackedOrder(order_number='1', status=STATUSES['pending'], status_update=init_dt,
                             submit=False, order_guid=guid)
        sap_order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='ONCLIENT',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100
                    )
                )
            ]
        ))
        await controller.track_order(tracked_order=track, sap_order=sap_order, service_order=service_order)
        assert not AdmitadService.postback_order.called
        tested_track = await service.get_track(order_number='1')
        assert not tested_track.get_submit()
        assert tested_track.get_status() == STATUSES['to_send']

    async def test_dont_send_order_because_of_date(self, controller: AdmitadController,
                                                   service: AdmitadService,
                                                   clear_table, mock_service_postback):
        init_dt = datetime.datetime.now() + datetime.timedelta(days=1)
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        track = TrackedOrder(order_number='1', status=STATUSES['to_send'], status_update=init_dt,
                             submit=False, order_guid=guid)
        await service.create_or_update_track(track=track)
        sap_order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='ONCLIENT',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100,
                        sale=100
                    )
                )
            ]
        ))
        await controller.track_order(tracked_order=track, sap_order=sap_order, service_order=service_order)
        assert not AdmitadService.postback_order.called
        tested_track = await service.get_track(order_number='1')
        assert not tested_track.get_submit()
        assert tested_track.get_status() == STATUSES['to_send']

    async def test_send_ok(self, controller: AdmitadController,
                           service: AdmitadService,
                           clear_table, mock_service_postback):
        init_dt = datetime.datetime.now() - datetime.timedelta(days=settings.ADMITAD_TERMINAL_WAIT_TIME_DAYS + 2)
        guid = 'f4b5971b-918a-45d8-a6ce-7b712f9baf2f'
        track = TrackedOrder(order_number='1', status=STATUSES['to_send'], status_update=init_dt,
                             submit=False, order_guid=guid)
        await service.create_or_update_track(track=track)
        sap_order = ClientSapOrder(data=dict(
            ITEMS=[
                dict(
                    COURIER_STATUS='ONCLIENT',
                    PRODUCT_CODE='1'
                )
            ],
            NUMBER='1',
            GUID=guid
        ))
        service_order = OrderDTO(data=dict(
            prices=dict(
                total=100
            ),
            positions=[
                dict(
                    productItemId='1',
                    price=dict(
                        base=100,
                        sale=100
                    )
                )
            ]
        ))
        await controller.track_order(tracked_order=track, sap_order=sap_order, service_order=service_order)
        assert AdmitadService.postback_order.called
        tested_track = await service.get_track(order_number='1')
        assert tested_track.get_submit()
        assert tested_track.get_status() == STATUSES['approved']
