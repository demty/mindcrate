import pytest
import datetime
from services.admitad.repository import AdmitadInternalRepository
from services.admitad.constants import STATUSES


@pytest.fixture(scope='function')
async def track_submit(app, clear_table):
    await app.pool.execute(
        'Insert into public.admitad_tracks (order_guid, order_number, status, submit, status_updated_at)'
        f"values "
        f"('1', '1', '{STATUSES['pending']}', FALSE, current_timestamp),"
        f"('2', '2', '{STATUSES['declined']}', FALSE, current_timestamp),"
        f"('3', '3', '{STATUSES['declined']}', FALSE, current_timestamp),"
        f"('4', '4', '{STATUSES['approved']}', TRUE , current_timestamp),"
        f"('5', '5', '{STATUSES['pending']}', TRUE, current_timestamp),"
        f"('6', '6', '{STATUSES['approved']}', TRUE, current_timestamp)"
    )
    yield None


@pytest.fixture(scope='function')
async def clear_table(app):
    yield None
    await app.pool.execute(
        'delete from public.admitad_tracks where id > 0'
    )


@pytest.mark.usefixtures('app_client', 'app_server')
class TestViews(object):
    class TestCreateTrack:
        async def test_create(self, app, clear_table):
            r = AdmitadInternalRepository(pool=app.pool)
            await r.create_or_update_track(
                order_number='1',
                order_guid='1',
                status=STATUSES['declined'],
                submit=False,
                update_date=datetime.datetime.now()
            )
            res = await r.get_order_status('1')
            assert res
            res_dict = dict(res)
            assert res_dict['order_number'] == '1'
            assert res_dict['status'] == STATUSES['declined']

        async def test_update(self, app, clear_table):
            r = AdmitadInternalRepository(pool=app.pool)
            await r.create_or_update_track(
                order_number='1',
                order_guid='1',
                status=STATUSES['declined'],
                submit=False,
                update_date=datetime.datetime.now()
            )
            res = await r.get_order_status('1')
            assert res
            res_dict = dict(res)
            assert res_dict['order_number'] == '1'
            assert res_dict['status'] == STATUSES['declined']
            r = AdmitadInternalRepository(pool=app.pool)
            await r.create_or_update_track(
                order_number='1',
                order_guid='1',
                status=STATUSES['approved'],
                submit=False,
                update_date=datetime.datetime.now()
            )
            res = await r.get_order_status('1')
            assert res
            res_dict = dict(res)
            assert res_dict['order_number'] == '1'
            assert res_dict['status'] == STATUSES['approved']

    class TestGet:
        async def test_exists(self, app, track_submit):
            r = AdmitadInternalRepository(pool=app.pool)
            res = await r.get_order_status('4')
            assert dict(res)['order_number'] == '4'
            assert res

        async def not_exists(self, app, track_submit):
            r = AdmitadInternalRepository(pool=app.pool)
            res = await r.get_order_status('not_exists')
            assert res is None

    class TestGetNotSubmitted:
        async def test_get_one_status(self, app, track_submit):
            r = AdmitadInternalRepository(pool=app.pool)
            res = await r.get_not_submitted_tracks(statuses=[STATUSES['pending']])
            assert len(res) == 1
            assert dict(res[0]).get('order_guid') == '1'

        async def test_get_two_statuses(self, app, track_submit):
            r = AdmitadInternalRepository(pool=app.pool)
            res = await r.get_not_submitted_tracks(statuses=[STATUSES['declined']])
            assert len(res) == 2
            assert dict(res[0]).get('order_guid') in ['2', '3']

        async def test_no_records(self, app, track_submit):
            r = AdmitadInternalRepository(pool=app.pool)
            res = await r.get_not_submitted_tracks(statuses=[STATUSES['approved']])
            assert len(res) == 0
