from aiohttp.client import ClientSession
from asyncpg.pool import Pool
from services.admitad.controller import AdmitadController
from services.admitad.repository import AdmitadInternalRepository, AdmitadRepository, AdmitadRepositoryMock, \
    MockRepository
from services.admitad.service import AdmitadService
from settings import ADMITAD_MODE


class ControllerFactory:
    """Фабрика контроллеров"""

    @classmethod
    def get_controller(cls, session: ClientSession, pool: Pool) -> AdmitadController:
        """Получить контроллер"""
        if ADMITAD_MODE == 'prod':
            repo = AdmitadRepository(session=session)
            internal_repo = AdmitadInternalRepository(pool=pool)
        else:
            repo = AdmitadRepositoryMock(session=session)
            if ADMITAD_MODE == 'db_debug':
                internal_repo = AdmitadInternalRepository(pool=pool)
            else:
                internal_repo = MockRepository()
        s = AdmitadService(repository=repo, internal_repository=internal_repo)
        c = AdmitadController(service=s)
        return c
