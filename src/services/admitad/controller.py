import datetime
import logging
from typing import List

import settings
from internal_services.order.entities import OrderDTO
from sap_client.entities import ClientSapOrder
from services.admitad.constants import STATUSES, TERMINAL_STATES, TERMINAL_STATES_DICT
from services.admitad.entities import TrackedOrder
from services.admitad.exceptions import OrderNotTracked
from services.admitad.service import AdmitadService

logger = logging.getLogger('admitad.controller')


class AdmitadController:
    """Контроллер адмитада"""

    def __init__(self, service: AdmitadService):
        """Конструктор"""
        self._service = service

    async def postback_order(self, sap_order: ClientSapOrder, service_order: OrderDTO):
        """
        Отследить в адмитад заказ
        @:param initial_state - Флаг первый ли вызов для заказа, если первый - статус заказа только
        """
        order_total_amount = service_order.get_total_price()
        reward = await self._get_order_reward(sap_order=sap_order, service_order=service_order)
        order_id = sap_order.get_client_number()
        if await self.check_if_order_in_terminal_stage(sap_order=sap_order):
            on_client_amount = await self._count_amount(sap_order=sap_order, service_order=service_order)
            status = await self._get_order_terminal_state(sap_order=sap_order)
            await self._service.postback_order(
                order_id=order_id,
                status=status,
                reward=reward,
                comment=await self._get_comment_from_status(status=status, order_total_amount=order_total_amount,
                                                            order_on_client_amount=on_client_amount),
                amount=on_client_amount
            )
        else:
            status = STATUSES['pending']
            await self._service.postback_order(
                order_id=order_id,
                status=status,
                reward=reward,
                comment=await self._get_comment_from_status(status=status, order_total_amount=order_total_amount,
                                                            order_on_client_amount=0),
                amount=order_total_amount
            )

    async def _get_order_reward(self, sap_order: ClientSapOrder, service_order: OrderDTO) -> float:
        """Получить размер награды для поставщика заказа"""
        return 0.1

    async def _count_amount(self, sap_order: ClientSapOrder, service_order: OrderDTO) -> float:
        """Подсчитать выкупленную сумму"""
        sap_result = dict()
        total_sum = 0
        for pos in sap_order.get_items():
            if pos.get_courier_status() == TERMINAL_STATES_DICT['onclient']:
                sap_result[pos.get_item_product_code()] = 1
            else:
                sap_result[pos.get_item_product_code()] = 0
        for pos in service_order.get_raw_positions():
            if pos['productItemId'] in sap_result and sap_result[pos['productItemId']]:
                total_sum += pos['price']['sale']
        return total_sum

    async def _get_order_terminal_state(self, sap_order: ClientSapOrder) -> str:
        on_client = []
        returned = []
        for pos in sap_order.get_items():
            if pos.get_courier_status() == TERMINAL_STATES_DICT['onclient']:
                on_client.append(pos)
            elif pos.get_courier_status() == TERMINAL_STATES_DICT['returned']:
                returned.append(pos)
        if on_client:
            return STATUSES['approved']
        return STATUSES['declined']

    async def _get_comment_from_status(self, status: str, order_total_amount: float,
                                       order_on_client_amount: float) -> str:
        """Получить комментарий для статуса"""
        if status == STATUSES['approved']:
            if order_total_amount != order_on_client_amount:
                return 'Частично выкуплено'
            else:
                return 'Полность выкуплен'
        elif status == STATUSES['declined']:
            return 'Полный возврат'
        else:
            return 'В процессе доставки'

    async def check_if_order_in_terminal_stage(self, sap_order: ClientSapOrder) -> bool:
        """Проверить в конечной ли стадии заказ"""
        for pos in sap_order.get_items():
            if pos.get_courier_status() not in TERMINAL_STATES:
                return False
        return True

    async def store_track_order(self, sap_order: ClientSapOrder):
        """Сохранить заказ в БД"""
        try:
            prev_track = await self._service.get_track(order_number=sap_order.get_client_number())
        except OrderNotTracked:
            prev_track = None
        status: str = STATUSES['pending']
        if not prev_track:
            prev_track = await self._service.generate_track_entity(
                status=status, order_number=sap_order.get_client_number(), order_guid=sap_order.get_guid()
            )
        else:
            prev_track.set_status(status)
            prev_track.set_submit(False)
        await self._service.create_or_update_track(track=prev_track)

    async def send_order(self,
                         sap_order: ClientSapOrder,
                         service_order: OrderDTO,
                         new_status: str):
        """Отправить заказ в адмитад"""
        reward = await self._get_order_reward(sap_order=sap_order, service_order=service_order)
        order_total_amount = service_order.get_total_price()
        on_client_amount = await self._count_amount(sap_order=sap_order, service_order=service_order)
        await self._service.postback_order(
            order_id=sap_order.get_client_number(),
            status=new_status,
            reward=reward,
            comment=await self._get_comment_from_status(status=new_status,
                                                        order_total_amount=order_total_amount,
                                                        order_on_client_amount=on_client_amount),
            amount=on_client_amount
        )

    async def track_order(self,
                          tracked_order: TrackedOrder,
                          sap_order: ClientSapOrder,
                          service_order: OrderDTO):
        """Отследить заказ: отправить его или пометить на отправку"""
        if tracked_order.get_submit():
            logger.debug(f'Order {tracked_order.get_order_number()} have already sent to admitad')
            return
        order_in_terminal_stage = await self.check_if_order_in_terminal_stage(sap_order=sap_order)
        delta_days = (datetime.datetime.now() - tracked_order.get_status_update_dt()).days
        new_status = ''

        if order_in_terminal_stage and tracked_order.get_status() == STATUSES['pending']:
            new_status: str = STATUSES['to_send']
        elif order_in_terminal_stage and tracked_order.get_status() == STATUSES['to_send'] \
                and delta_days >= settings.ADMITAD_TERMINAL_WAIT_TIME_DAYS:
            new_status: str = await self._get_order_terminal_state(sap_order=sap_order)
            await self.send_order(sap_order=sap_order,
                                  service_order=service_order,
                                  new_status=new_status)
            tracked_order.set_submit(True)

        if new_status:
            tracked_order.set_status(new_status)
            tracked_order.update_status_update_dt()
            await self._service.create_or_update_track(track=tracked_order)

    async def get_unsubmitted_tracks(self, statuses=(STATUSES['pending'],)) -> List[TrackedOrder]:
        """Получить неотправленные треки"""
        return await self._service.get_unsubmitted_tracks(statuses=statuses)
