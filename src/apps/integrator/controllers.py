import logging
from typing import Optional

from aiohttp import ClientSession
from timeit import default_timer as timer
from lobster_client import LobsterClient
from lobster_client.entities import LobsterConfig

import settings
from apps.integrator.operations.transfer_segment.operations import SegmentTransfer
from apps.integrator.operations.update_lists import IntegrateUpdateListsManager
from apps.integrator.parser import get_user_phones_from_file
from apps.integrator.operations.update_users import get_boss
from apps.integrator.presenters import SegmentPresenter
from external_services.criteo import CriteoClient
from external_services.criteo.entities import Audience
from external_services.mindbox.controller import NewMindboxController
from external_services.rtb import RtbClient

logger = logging.getLogger(__name__)


class IntegrateLists(IntegrateUpdateListsManager):
    @classmethod
    def from_params(cls, session: ClientSession, lobster_config: LobsterConfig) -> 'IntegrateLists':
        return cls(session=session, lobster_config=lobster_config)

    async def update_lists_from_file(self, file_content: bytearray):
        start = timer()
        phones = get_user_phones_from_file(file_content)
        logger.info(f'total phones: {len(phones.keys())}')
        self.needs_to_proceed = len(phones)
        self._start_phone_workers()
        for phone in phones:
            await self.phones_queue.put(phone)
        await self._wait_for_result()
        await self._stop_workers()
        stop = timer()
        logger.info(f'time elapsed: {stop - start}')
        return self.errors


class IntegrateUpdateUsers:
    def __init__(self, session: ClientSession, lobster_config: LobsterConfig):
        self.lobster_c = LobsterClient.from_params(session=session, config=lobster_config)
        self.mb_c = NewMindboxController.from_params(session=session)

    @classmethod
    def from_params(cls, session: ClientSession, lobster_config: LobsterConfig) -> 'IntegrateUpdateUsers':
        return cls(session=session, lobster_config=lobster_config)

    async def update_users(self, file_content: bytearray):
        boss = get_boss(self.lobster_c, self.mb_c)
        await boss.perform_job(
            file_content=file_content
        )
        return boss.get_errors()


class SegmentsController:
    def __init__(self, session: ClientSession, lobster_config: LobsterConfig):
        self._criteo_client = CriteoClient(
            base_url=settings.CRITEO_API_ENDPOINT, client_id=settings.CRITEO_CLIENT_ID, session=session,
            client_secret=settings.CRITEO_CLIENT_SECRET, advertiser_id=settings.CRITEO_ADVERTISER_ID
        )
        self._rtb_client = RtbClient(session=session, company_hash=settings.RTB_COMPANY_HASH,
                                     base_url=settings.RTB_API_ENDPOINT)
        self._lobster_client = LobsterClient.from_params(session=session, config=lobster_config)
        self._mindbox_controller = NewMindboxController.from_params(session=session)

    @classmethod
    def from_params(cls, session: ClientSession, lobster_config: LobsterConfig) -> 'SegmentsController':
        return cls(session=session, lobster_config=lobster_config)

    async def transfer(self, mindbox_endpoint: str, criteo_audience_name: str) -> str:
        segment_transfer = SegmentTransfer(lobster_client=self._lobster_client, rtb_client=self._rtb_client,
                                           mindbox_controller=self._mindbox_controller,
                                           criteo_client=self._criteo_client)
        audience = await self._get_or_create_audience(criteo_audience_name=criteo_audience_name)
        boss = await segment_transfer.prepare_boss(mindbox_endpoint=mindbox_endpoint,
                                                   criteo_audience=audience)
        await boss.perform_job()
        return SegmentPresenter.get_transfer_answer(boss)

    async def clear_criteo_audience(self, criteo_audience_name: str) -> str:
        audience = await self._get_or_create_audience(criteo_audience_name=criteo_audience_name)
        await self._criteo_client.clear_audience(audience)
        return SegmentPresenter.clear_audience_answer()

    async def delete_criteo_audience(self, criteo_audience_name: str) -> str:
        audience = await self._get_or_create_audience(criteo_audience_name=criteo_audience_name)
        await self._criteo_client.delete_audience(audience=audience)
        return SegmentPresenter.delete_audience_answer()

    async def get_audiences(self) -> str:
        auds = await self._criteo_client.get_audiences()
        return SegmentPresenter.get_audiences_answer(audiences=auds)

    async def _get_or_create_audience(self, criteo_audience_name: str) -> Audience:
        criteo_audience_name_lowered = criteo_audience_name.lower()
        audience = await self._get_audience(criteo_audience_name_lowered)
        if not audience:
            audience = await self._create_audience(criteo_audience_name_lowered)
        return audience

    async def _create_audience(self, criteo_segment_name: str):
        res_aud = await self._criteo_client.create_audience(name=criteo_segment_name, description=criteo_segment_name)
        return res_aud

    async def _get_audience(self, criteo_segment_name: str) -> Optional[Audience]:
        currently_created_auds = await self._criteo_client.get_audiences()
        for aud in currently_created_auds:
            if aud.name == criteo_segment_name:
                return aud
