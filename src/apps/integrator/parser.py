import csv
import re
from typing import Dict, Any

re_p = re.compile('([7|8][0-9]{10})')
re_s = re.compile('^(7|8)')


def get_user_phones_from_file(file_content: bytearray) -> Dict[str, Any]:
    phones = {}
    # with open(file_content, newline='') as csvfile:
    spamreader = csv.reader(file_content.decode('utf-8').split('\r\n'), delimiter=',', quotechar='|')
    for row in spamreader:
        if row and row[0]:
            phone = re_p.match(str(row[0]))
            phone = phone and phone.group(0)
            if phone:
                phone = re_s.sub('7', phone)
                phones[phone] = None
    return phones
