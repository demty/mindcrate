import pytest
from aiohttp import ClientSession
from lobster_client.entities import LobsterConfig
from pytest_mock import MockFixture
from runnector import AbstractBoss

from apps.integrator.controllers import SegmentsController
from apps.integrator.operations.transfer_segment.operations import SegmentTransfer
from external_services.criteo.tests.factories import AudienceFactory

MINDBOX_ENDPOINT = 'test'


async def stub(*args, **kwargs):
    pass


@pytest.fixture()
def segment_controller(lobster_client, rtb_client, mindbox_controller, criteo_client):
    return SegmentsController.from_params(session=ClientSession(),
                                          lobster_config=LobsterConfig(api_url='some_url.com', service_slug='test',
                                                                       referrer='test'))


class TestTransferSegmentControllerTransfer:
    async def test_no_audience_exists_in_criteo(self, mocker: MockFixture, segment_controller: SegmentsController):
        aud_not_existing_name = AudienceFactory().name + '_'

        async def get_auds(*args, **kwargs):
            return [
                AudienceFactory(), AudienceFactory()
            ]

        async def create_auds(*args, **kwargs):
            return AudienceFactory()

        mocker.patch('external_services.criteo.client.CriteoClient.get_audiences', side_effect=get_auds)
        mocker.patch('external_services.criteo.client.CriteoClient.create_audience', side_effect=create_auds)
        mocker.patch('runnector.boss.AbstractBoss.perform_job', side_effect=stub)
        await segment_controller.transfer(mindbox_endpoint=MINDBOX_ENDPOINT, criteo_audience_name=aud_not_existing_name)
        assert segment_controller._criteo_client.get_audiences.called
        assert segment_controller._criteo_client.create_audience.called
        assert AbstractBoss.perform_job.called

    async def test_audience_for_criteo_exists(self, mocker: MockFixture, segment_controller: SegmentsController):
        created_aud = AudienceFactory()

        async def get_auds(*args, **kwargs):
            return [
                created_aud, AudienceFactory()
            ]

        mocker.patch('external_services.criteo.client.CriteoClient.get_audiences', side_effect=get_auds)
        mocker.patch('external_services.criteo.client.CriteoClient.create_audience', side_effect=stub)
        mocker.patch('runnector.boss.AbstractBoss.perform_job', side_effect=stub)
        await segment_controller.transfer(mindbox_endpoint=MINDBOX_ENDPOINT, criteo_audience_name=created_aud.name)
        assert segment_controller._criteo_client.get_audiences.called
        assert not segment_controller._criteo_client.create_audience.called
        assert AbstractBoss.perform_job.called
