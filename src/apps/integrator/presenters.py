from typing import List

from apps.integrator.operations.transfer_segment.operators import LoadSegmentBoss
from external_services.criteo.entities import Audience


class SegmentPresenter:
    @classmethod
    def get_audiences_answer(cls, audiences: List[Audience]) -> str:
        text = 'Существующие аудитории:\n'
        text += '\n'.join(f'{x.name}' for x in audiences)
        return text

    @classmethod
    def get_transfer_answer(cls, boss: LoadSegmentBoss):
        if boss.errors:
            res = 'Трансфер прошел с ошибками'
        else:
            res = 'Трансфер прошел успешно'
        return res

    @classmethod
    def clear_audience_answer(cls):
        return 'Список пользователей аудитории успешно очищен'

    @classmethod
    def delete_audience_answer(cls):
        return 'Аудитория успешно удалена'
