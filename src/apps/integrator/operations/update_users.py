import logging
from typing import AsyncIterable, Optional, Tuple, Any, NoReturn

from lobster_client import LobsterClient
from runnector import AbstractBoss, AbstractWorker, WorkTask

from apps.integrator.parser import get_user_phones_from_file
from external_services.mindbox.controller import NewMindboxController
from external_services.mindbox.entities import MindboxRequestParams
from external_services.mindbox.presenter_mixins import UserForceEditFields
from utils.helpers import take_by


logger = logging.getLogger(__name__)
PHONES_CHUNK_SIZE = 200
m_p = MindboxRequestParams(customer_ip=None, device_uuid=None, user_agent=None)


class UpdateUsersBoss(AbstractBoss):
    async def prepare_data_for_workers(self, file_content: bytearray) -> AsyncIterable[WorkTask]:
        logger.debug('preparing data')
        phones = get_user_phones_from_file(file_content)
        for chunk in take_by(PHONES_CHUNK_SIZE, phones):
            yield WorkTask(task_args=chunk)
        logger.debug('data prepared')


class UserGetterWorker(AbstractWorker):

    work_name = 'UserGetterWorker'

    def __init__(self, lobster_client: LobsterClient, *args, **kwargs):
        self._lobster_client = lobster_client
        super().__init__(*args, **kwargs)

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        logger.debug(f'{self.id} - searching users {work_task.task_args}')
        users = await self._lobster_client.search_users(
            phones=work_task.task_args
        )
        logger.debug(f'{self.id} - found users')
        return users


class UserUpdateWorker(AbstractWorker):

    work_name = 'UserUpdateWorker'

    def __init__(self, mindbox_controller: NewMindboxController, *args, **kwargs):
        self._mb_controller = mindbox_controller
        super().__init__(*args, **kwargs)

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        logger.debug(f'{self.id} - pushing to mb users {work_task.task_args}')
        for user in work_task.task_args:
            logger.debug(f'{self.id} - pushing to mb user {user.get_phone()}')
            await self._mb_controller.update_user_by_phone(
                request_params=m_p, user=user, update_fields={UserForceEditFields.userKey}
            )
            logger.debug(f'{self.id} - pushed to mb user {user.get_phone()}')


def get_boss(lobster_client: LobsterClient, mindbox_controller: NewMindboxController) -> UpdateUsersBoss:
    w1 = UserGetterWorker.spawn(count=20, produces=True, lobster_client=lobster_client)
    w2 = UserUpdateWorker.spawn(count=20, produces=False, mindbox_controller=mindbox_controller)
    w2.consume(w1)
    boss = UpdateUsersBoss()
    boss.setup_workers(
        w1
    )
    return boss
