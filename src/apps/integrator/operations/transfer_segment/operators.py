import logging
from typing import Optional, Tuple, Any, NoReturn, List, AsyncIterable

from lobster_client import LobsterClient
from runnector import AbstractWorker, WorkTask, AbstractBoss

from external_services.criteo import CriteoClient
from external_services.criteo.entities import Audience, AudienceUsersOperation
from external_services.mindbox.controller import NewMindboxController
from external_services.rtb import RtbClient


logger = logging.getLogger(__name__)


class RtbWorker(AbstractWorker):

    def __init__(self, rtb_client: RtbClient, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._rtb_client = rtb_client

    @property
    def work_name(self) -> str:
        return 'rtb upload'

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        logger.info("RTB load started")
        user_ids = work_task.task_args
        logger.info("RTB load user_ids retrieved, to upload: %s" % len(user_ids))
        for user_id in user_ids:
            await self._rtb_client.tag_user(user_id=user_id)
        logger.info("RTB load user_ids tagged")


class LobsterLoadUsersWorker(AbstractWorker):

    def __init__(self, lobster_client: LobsterClient, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lobster_client = lobster_client

    @property
    def work_name(self) -> str:
        return 'lobster download users by phone'

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        logger.info("Lobster load started")
        phones: List[str] = [x[0] for x in work_task.task_args if x[0]]
        logger.info("Lobster load phones retrieved")
        user_ids = [x.get_user_id() for x in await self._lobster_client.search_users(phones=phones)]
        logger.info("Lobster load user_ids retrieved")
        return user_ids


class CriteoWorker(AbstractWorker):

    def __init__(self, criteo_audience: Audience, criteo_client: CriteoClient, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._criteo_aud = criteo_audience
        self._criteo_client = criteo_client

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        logger.info("Criteo load started")
        emails = [x[1] for x in work_task.task_args if x[1]]
        logger.info("Criteo load emails retrieved")
        await self._criteo_client.add_or_remove_users_to_audience(
            operation=AudienceUsersOperation.add, audience=self._criteo_aud, users_emails=emails
        )
        logger.info("Criteo load emails uploaded")

    @property
    def work_name(self) -> str:
        return 'criteo data upload'


class LoadSegmentBoss(AbstractBoss):
    def __init__(self, mindbox_controller: NewMindboxController, mindbox_operation_name: str):
        super().__init__()
        self._mbc = mindbox_controller
        self._mb_op_name = mindbox_operation_name

    async def prepare_data_for_workers(self, *args, **kwargs) -> AsyncIterable[WorkTask]:
        start_page = 0
        offset = kwargs.get('offset', 1000)
        customers_found = True
        logger.info('Load segments started')
        while customers_found:
            logger.info('Loading segments for sp: %s, offset: %s', start_page, offset)
            customers = await self._mbc.load_customers_by_segment(
                operation_name=self._mb_op_name, start_page=start_page, offset=offset
            )
            logger.info('Loading segments for sp: %s, offset: %s', start_page, offset)
            customers_found = customers is not None and len(customers) > 0
            logger.info('Loading segments data found: %s', customers_found)
            start_page += offset
            if customers_found:
                logger.info('Loading segments found %s customers', len(customers))
                yield WorkTask(
                    task_args=[(x.phone, x.email) for x in customers]
                )
            if customers and len(customers) < offset:
                customers_found = False
        logger.info('Boss prepared data')
