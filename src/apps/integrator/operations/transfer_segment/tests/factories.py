import factory
from lobster_client.entities import UserEntity


class UserEntityFactory(factory.Factory):
    class Meta:
        model = UserEntity

    data = factory.Sequence(lambda n: {'user_id': n})
