import pytest
from pytest_mock import MockFixture

from apps.integrator.operations.transfer_segment.operations import SegmentTransfer
from external_services.criteo.tests.factories import AudienceFactory

MINDBOX_ENDPOINT = 'test'


@pytest.fixture()
def transfer_segment_operation(lobster_client, rtb_client, mindbox_controller, criteo_client):
    return SegmentTransfer(lobster_client=lobster_client, rtb_client=rtb_client, mindbox_controller=mindbox_controller,
                           criteo_client=criteo_client)


class TestTransferSegmentOperation:
    async def test_boss_created(self, transfer_segment_operation: SegmentTransfer):
        boss = await transfer_segment_operation.prepare_boss(mindbox_endpoint=MINDBOX_ENDPOINT,
                                                             criteo_audience=AudienceFactory())
        assert boss
