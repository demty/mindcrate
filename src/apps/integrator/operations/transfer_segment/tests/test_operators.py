from lobster_client import LobsterClient
from pytest_mock import MockFixture
from runnector import WorkersGroup, WorkTask

from apps.integrator.operations.transfer_segment.operators import LoadSegmentBoss, CriteoWorker, RtbWorker, \
    LobsterLoadUsersWorker
from apps.integrator.operations.transfer_segment.tests.factories import UserEntityFactory
from external_services.criteo import CriteoClient
from external_services.criteo.tests.factories import AudienceFactory
from external_services.mindbox.tests.factories import CustomerInfoFactory
from external_services.rtb import RtbClient

START_PAGE = 0
OFFSET = 2
OPERATION = 'test'

BOSS_DATA = ('some_phone', 'test@test.com'), ('some_phone2', 'test2@test2.com')


async def stub(*args, **kwargs):
    pass


class TestBoss:
    async def test_two_pages_of_data_two_tasks_produced(self, mocker: MockFixture, mindbox_controller):
        c1 = CustomerInfoFactory()
        c2 = CustomerInfoFactory()
        c3 = CustomerInfoFactory()

        async def ret_f(operation_name: str, start_page: int, offset: int):
            if start_page == 0:
                return [
                    c1, c2
                ]
            elif start_page == 2:
                return [
                    c3
                ]
            else:
                raise Exception(f'too much calls, allowed pages: 0, 1, called with page: {start_page}')

        mocker.patch('external_services.mindbox.controller.NewMindboxController.load_customers_by_segment',
                     side_effect=ret_f)
        b = LoadSegmentBoss(mindbox_controller=mindbox_controller, mindbox_operation_name=OPERATION)
        res = []
        async for x in b.prepare_data_for_workers(offset=2):
            res.append(x)
        assert len(res) == 2
        assert len(res[0].task_args) == 2
        assert len(res[1].task_args) == 1
        assert res[0].task_args[0] == (c1.phone, c1.email)
        assert res[0].task_args[1] == (c2.phone, c2.email)
        assert res[1].task_args[0] == (c3.phone, c3.email)


class TestCriteoWorker:
    async def test_task_with_email(self, mocker: MockFixture, criteo_client: CriteoClient):
        mocker.patch('external_services.criteo.client.CriteoClient.add_or_remove_users_to_audience',
                     side_effect=stub)
        w_g = WorkersGroup(produces=False)
        audience = AudienceFactory()
        w = CriteoWorker(criteo_client=criteo_client, workers_group=w_g, criteo_audience=audience)
        await w.work(
            work_task=WorkTask(task_args=BOSS_DATA)
        )
        assert w._criteo_client.add_or_remove_users_to_audience.called
        assert w._criteo_client.add_or_remove_users_to_audience.call_args[1]['users_emails'] == \
            [x[1] for x in BOSS_DATA]


class TestRtbWorker:
    async def test_rtb_called_with_right_params(self, mocker: MockFixture, rtb_client: RtbClient):
        mocker.patch('external_services.rtb.client.RtbClient.tag_user', side_effect=stub)
        w_g = WorkersGroup(produces=False)
        w = RtbWorker(rtb_client=rtb_client, workers_group=w_g)
        data = (1, 2, 3)
        await w.work(
            work_task=WorkTask(task_args=data)
        )
        assert rtb_client.tag_user.called
        assert rtb_client.tag_user.call_count == 3


class TestLobsterWorker:
    async def test_lobster_called_with_right_params(self, mocker: MockFixture, lobster_client: LobsterClient):
        users = [UserEntityFactory(), UserEntityFactory()]

        async def get_users(*args, **kwargs):
            return users

        mocker.patch('lobster_client.client.LobsterClient.search_users', side_effect=get_users)
        w_g = WorkersGroup(produces=False)
        w = LobsterLoadUsersWorker(lobster_client=lobster_client, workers_group=w_g)
        res = await w.work(
            work_task=WorkTask(task_args=BOSS_DATA)
        )
        assert lobster_client.search_users.called
        assert len(res) == 2
        assert [x.get_user_id() for x in users] == res
