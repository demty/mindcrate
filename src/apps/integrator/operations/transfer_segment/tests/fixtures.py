import pytest
from aiohttp import ClientSession
from lobster_client import LobsterClient
from lobster_client.entities import LobsterConfig


@pytest.fixture()
async def lobster_client():
    s = ClientSession()
    lc = LobsterClient.from_params(config=LobsterConfig(
        api_url='test-my-api.com', referrer='test_ref', service_slug='test_slug'
    ), session=s)
    yield lc
    await s.close()
