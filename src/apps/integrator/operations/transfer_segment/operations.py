import logging

from lobster_client import LobsterClient

from apps.integrator.operations.transfer_segment.operators import RtbWorker, LobsterLoadUsersWorker, CriteoWorker, \
    LoadSegmentBoss
from external_services.criteo import CriteoClient
from external_services.criteo.entities import Audience
from external_services.mindbox.controller import NewMindboxController
from external_services.rtb import RtbClient

logger = logging.getLogger(__name__)


class SegmentTransfer:
    def __init__(self, lobster_client: LobsterClient, criteo_client: CriteoClient,
                 mindbox_controller: NewMindboxController, rtb_client: RtbClient):
        self._lc = lobster_client
        self._cc = criteo_client
        self._mc = mindbox_controller
        self._rc = rtb_client

    async def prepare_boss(self, mindbox_endpoint: str, criteo_audience: Audience) -> LoadSegmentBoss:
        boss = LoadSegmentBoss(mindbox_controller=self._mc, mindbox_operation_name=mindbox_endpoint)
        w1 = RtbWorker.spawn(count=20, rtb_client=self._rc)
        w2 = LobsterLoadUsersWorker.spawn(count=10, produces=True, lobster_client=self._lc)
        w3 = CriteoWorker.spawn(count=10, criteo_client=self._cc, criteo_audience=criteo_audience)
        w1.consume(w2)
        boss.setup_workers(w3, w2)
        return boss
