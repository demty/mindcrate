import logging
from asyncio import shield

import settings
from apps.tracker.exceptions import WrongVersion
from apps.integrator.controllers import IntegrateLists, IntegrateUpdateUsers
from utils.error_handle_decorators import async_error_processing
from utils.response_formatters import google_json_response

logger = logging.getLogger(__name__)


@async_error_processing(logger=logger)
async def reset_wish_and_wait_lists(request):
    api_version = request.match_info.get('api_version')
    if api_version != '1.0':
        raise WrongVersion('Неверная версия апи, доступно: 1.0')
    if request.headers.get('token') != settings.MINDBOX_ACCESS_KEY:
        raise WrongVersion('Неверный токен')
    integrator = IntegrateLists(session=request.app.session, lobster_config=request.app.lobster_config)
    data = await request.post()
    phones_file = data['phones'].file
    content = phones_file.read()
    res = await shield(integrator.update_lists_from_file(file_content=content))
    return google_json_response(data={'errors': res}, status=200, api_version=api_version)


@async_error_processing(logger=logger)
async def update_users(request):
    api_version = request.match_info.get('api_version')
    if api_version != '1.0':
        raise WrongVersion('Неверная версия апи, доступно: 1.0')
    if request.headers.get('token') != settings.MINDBOX_ACCESS_KEY:
        raise WrongVersion('Неверный токен')
    integrator = IntegrateUpdateUsers(session=request.app.session, lobster_config=request.app.lobster_config)
    data = await request.post()
    phones_file = data['phones'].file
    content = phones_file.read()
    res = await shield(integrator.update_users(file_content=content))
    return google_json_response(data={'errors': res}, status=200, api_version=api_version)
