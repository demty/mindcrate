from typing import Dict, List


class EsbEvent:
    """Событие из esb"""

    def __init__(self, data: Dict):
        """Конструктор"""
        self._data = data

    def get_sender(self) -> str:
        """Получить отправителя"""
        return self._data['sender']

    def get_object(self) -> str:
        """Получить измененный объект"""
        return self._data['object']

    def get_action(self) -> str:
        """Получить название события"""
        return self._data['action']

    def get_data(self) -> List[str]:
        """Получить данные события"""
        res_data = self._data['data']
        if type(res_data) != list:
            return [res_data]
        return res_data


class EsbEventFactory:
    """Фабрика событий"""

    @classmethod
    def get_event(cls, data: Dict) -> EsbEvent:
        """Получить объект события"""
        return EsbEvent(data=data)
