from utils.exceptions import BaseAppException


class InsufficientData(BaseAppException):
    """Передано недостаточно данных"""

    code = 403
    reason = 'insufficientData'
    internal_code = '403-tracker-1'


class WrongVersion(BaseAppException):
    """Неверная версия апи"""

    code = 403
    reason = 'wrongVersion'
    internal_code = '403-version-1'
