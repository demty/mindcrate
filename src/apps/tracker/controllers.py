import logging
from typing import Dict, Tuple

import settings
from aiohttp.client import ClientSession
from aiohttp.web_request import BaseRequest
from alligator_client.controller_factory import ControllerFactory as AlligatorFactory
from lobster_client import LobsterClient
from lobster_client.entities import UserEntity, LobsterConfig
from apps.tracker.entities import EsbEventFactory
from apps.tracker.exceptions import InsufficientData
from asyncpg.pool import Pool
from apps.tracker.services import MindboxTrackerService, GoogleTrackerService
from external_services.google_analytics.factories import ControllerFactory as GoogleFactory
from external_services.mindbox.controller import NewMindboxController
from external_services.mindbox.entities import MindboxRequestParams, RegisterParams
from external_services.mindbox.factories import ControllerFactory as MindboxFactory, \
    MindboxRequestParamsFactory
from internal_services.btl.factories import ControllerFactory as BtlFactory
from internal_services.massmedia.controller_factory import ControllerFactory as MassmediaFactory
from internal_services.order.controller_factory import ControllerFactory as OrderFactory
from internal_services.order.entities import OrderDTO
from internal_services.waitlist.controller_factory import ControllerFactory as WaitlistFactory
from internal_services.wishlist.controller_factory import ControllerFactory as WishlistFactory
from sap_client.controller_factory import ControllerFactory
from services.admitad.constants import STATUSES
from services.admitad.controller_factory import ControllerFactory as AdmitadFactory

logger = logging.getLogger(__name__)
logger_info = logging.getLogger('sentry_info')


class MindboxTrackerController:
    """Контроллер трекера майндбокса"""

    def __init__(self, service: MindboxTrackerService, session: ClientSession, pool: Pool,
                 lobster_config: LobsterConfig):
        """Конструктор"""
        self.service = service
        self.order_controller = OrderFactory.get_controller(session=session)
        self.btl_controller = BtlFactory.get_controller(session=session)
        self.mindbox_controller = MindboxFactory.get_controller(session=session)
        self.massmedia_controller = MassmediaFactory.get_controller(session=session)
        self.lobster_client = LobsterClient.from_params(session=session, config=lobster_config)
        self.wishlist_controller = WishlistFactory.get_controller(session=session)
        self.sap_controller = ControllerFactory.get_controller(session=session, sap_url=settings.SAP_API_ENDPOINT,
                                                               sap_login=settings.SAP_LOGIN,
                                                               sap_password=settings.SAP_PASSWORD)
        self.alligator_controller = AlligatorFactory.get_controller(
            session=session, site_url=settings.ENNERGIIA_ENDPOINT_EXTERNAL,
            alligator_url=settings.ALLIGATOR_API_ENDPOINT
        )
        self.admitad_controller = AdmitadFactory.get_controller(
            session=session, pool=pool
        )
        self.waitlist_controller = WaitlistFactory.get_controller(session=session)
        self.new_mb_controller = NewMindboxController.from_params(session=session)

    async def _get_alligator_params(self, data: Dict) -> Tuple[str, int, int]:
        svyabk = settings.BASE_SVYABK
        avail_zone = settings.BASE_AVAIL_ZONE
        price_zone = settings.BASE_PRICE_ZONE
        return svyabk, avail_zone, price_zone

    async def _get_request_data(self, request: BaseRequest) -> Dict:
        try:
            request_data = await request.json()
        except Exception:
            request_data = dict()
        if 'customerIp' not in request_data:
            request_data['customerIp'] = request.headers.get('X-Real-IP')
        return request_data

    async def _get_register_data(self, request_data: Dict) -> RegisterParams:
        return RegisterParams.from_params(email=request_data.get('email'), brand=request_data.get('brand', 'Ennergiia'))

    async def _get_request_params(self, request_data: Dict) -> MindboxRequestParams:
        customer_ip = request_data.get('customerIp')
        device_uuid = request_data.get('deviceUuid')
        user_agent = request_data.get('userAgent')
        request_params = MindboxRequestParamsFactory.create_request_params(
            user_agent=user_agent, customer_ip=customer_ip, device_uuid=device_uuid
        )
        return request_params

    async def _get_data_for_mindbox(
            self, request_data: Dict,
            needs_user_data: bool) -> Tuple[UserEntity or None, MindboxRequestParams, str]:
        """Получить общие данные для запрооса в майндбокс"""
        lobster_user = None
        user_id = request_data.get('userId')
        user_token = request_data.get('userToken')
        if needs_user_data and user_token and not user_id:
            token_data = await self.lobster_client.decode_token(user_token)
            user_id = str(token_data['user_id']) if token_data else None
        if user_id:
            lobster_user = await self.lobster_client.search_user(user_id=user_id)
        if not lobster_user:
            lobster_user = UserEntity(
                data={'email': '', 'phone': '', 'user_id': None, 'first_name': '', 'last_name': ''}
            )
        request_params = await self._get_request_params(request_data=request_data)
        return lobster_user, request_params, user_token

    async def _get_order_data_for_mindbox(self, request_data: Dict, order_guid: str) -> Tuple[UserEntity,
                                                                                              MindboxRequestParams,
                                                                                              OrderDTO]:
        """Данные для заказа и корзины нужны одни и те же"""
        lobster_user, request_params, user_token = await self._get_data_for_mindbox(
            request_data=request_data, needs_user_data=True
        )
        order = await self.order_controller.get_order(order_guid=order_guid, user_token=user_token)
        return lobster_user, request_params, order

    async def update_beloved_brands(self, request):
        """Обновить любимые бренды"""
        data = await self._get_request_data(request)
        lobster_user, request_params, user_token = await self._get_data_for_mindbox(
            request_data=data, needs_user_data=True
        )
        beloved_brand_codes = await self.wishlist_controller.get_beloved_brands(user_token=user_token)
        svyabk, avail_zone, price_zone = await self._get_alligator_params(data)
        beloved_brands = await self.alligator_controller.get_brands_by_codes(
            brand_codes=[str(x.get_brand_code()) for x in beloved_brand_codes], svyabk=svyabk, avail_zone_id=avail_zone,
            price_zone_id=price_zone
        )
        await self.mindbox_controller.update_beloved_brands(lobster_user=lobster_user, request_params=request_params,
                                                            beloved_brands=beloved_brands)