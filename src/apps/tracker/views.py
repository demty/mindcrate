# import aioredis
import logging
from asyncio import shield

# import settings
from apps.tracker.exceptions import WrongVersion
from apps.tracker.factories import ControllerFactory
from utils.error_handle_decorators import async_error_processing
from utils.response_formatters import google_json_response

logger = logging.getLogger(__name__)


@async_error_processing(logger=logger)
async def track_cart(request):
    """
    Отслеживание корзины
    https://docs.google.com/document/d/1DcAYElYHl6JQ1m3fMvOfTu-VIqmFwdlh2CbypMg9hN4/edit#heading=h.y6bfy57kbwjx
    """
    api_version = request.match_info.get('api_version')
    if api_version != '1.0':
        raise WrongVersion('Неверная версия апи, доступно: 1.0')
    controller = ControllerFactory.get_mindbox_controller(pool=request.app.pool, session=request.app.session,
                                                          lobster_config=request.app.lobster_config)
    await shield(controller.track_cart_changes(request=request))
    return google_json_response(data={'result': 'ok'}, status=200, api_version=api_version)