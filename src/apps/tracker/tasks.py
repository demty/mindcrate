import asyncio
import logging

import settings
from aiohttp.web import Application
from apps.tracker.factories import ControllerFactory
from utils.tasks_synchronizer import check_if_task_can_be_taken, release_task

logger = logging.getLogger(__name__)


async def check_for_finished_orders(app: Application):
    """Таск для отправки завершенных заказов"""
    await asyncio.sleep(settings.ADMITAD_CHECK_JOB_EVERY_SECONDS)
    if await check_if_task_can_be_taken(check_for_finished_orders.__name__, app['guid']):
        logger.debug(f"Checking for unsubmitted order tracks in terminal state for "
                     f"{settings.ADMITAD_TERMINAL_WAIT_TIME_DAYS} days")
        factory = ControllerFactory.get_mindbox_controller(pool=app.pool, session=app.session)
        await factory.send_unsubmitted_tracks()
        await release_task(check_for_finished_orders.__name__, app_guid=app['guid'])
    else:
        logger.info("Task 'check_for_finished_orders' already taken")
