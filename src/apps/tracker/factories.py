from aiohttp.client import ClientSession
from lobster_client.entities import LobsterConfig

from apps.tracker.controllers import MindboxTrackerController, GoogleTrackerController
from apps.tracker.repository import MindboxRepository, GoogleRepository
from apps.tracker.services import MindboxTrackerService, GoogleTrackerService
from asyncpg.pool import Pool


class ControllerFactory:
    """Фабрика контроллеров трекера"""

    @classmethod
    def get_mindbox_controller(cls, pool: Pool, session: ClientSession,
                               lobster_config: LobsterConfig) -> MindboxTrackerController:
        """Получить контроллер mindbox"""
        r = MindboxRepository(pool=pool)
        s = MindboxTrackerService(repository=r)
        c = MindboxTrackerController(service=s, session=session, pool=pool, lobster_config=lobster_config)
        return c

    @classmethod
    def get_google_controller(cls, pool: Pool, session: ClientSession) -> GoogleTrackerController:
        """Получить контроллер google"""
        r = GoogleRepository(pool=pool)
        s = GoogleTrackerService(repository=r)
        c = GoogleTrackerController(service=s, session=session)
        return c
