from typing import Dict
from apps.tracker.repository import MindboxRepository, GoogleRepository


class MindboxTrackerService:
    """Сервис трекера майндбокса"""

    def __init__(self, repository: MindboxRepository):
        """Конструктор"""
        self._repo = repository

    async def save_tracking_data(self, data: Dict):
        """Сохранить данные, которые отправляются в майндбокс"""
        await self._repo.create_record(data=data)


class GoogleTrackerService:
    """Сервис трекера гугла"""

    def __init__(self, repository: GoogleRepository):
        """Конструктор"""
        self._repo = repository
