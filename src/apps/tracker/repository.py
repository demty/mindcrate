from typing import Dict

import ujson
from asyncpg.pool import Pool

MINDBOX_TBL_NAME = 'mindbox_data'


class MindboxRepository:
    """Репозиторий для сохранения данных майндбокса"""

    def __init__(self, pool: Pool):
        """Конструктор"""
        self.pool = pool

    async def create_record(self, data: Dict):
        """Создать запись"""
        record = {'data': ujson.dumps(data)}
        await self.pool.execute(
            f'INSERT INTO {MINDBOX_TBL_NAME}(data) values($1)',
            record
        )


class GoogleRepository:
    """Шоб було. Если нужны будут какие-то очереди для доотправки, то сюда"""

    def __init__(self, pool: Pool):
        """Конструктор"""
        self.pool = pool
