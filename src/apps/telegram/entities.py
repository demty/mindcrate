from dataclasses import dataclass
from typing import Any, Dict


@dataclass
class TelegramIncomingMessage:
    chat: int
    text: str

    @classmethod
    def from_telegram_dict(cls, data: Dict[str, Any]):
        return cls(
            chat=data['message']['chat']['id'],
            text=data['message']['text']
        )


@dataclass
class TelegramOutgoingMessage:
    chat: int
    text: str
