import logging

from apps.integrator.controllers import SegmentsController
from apps.telegram.entities import TelegramIncomingMessage
from apps.telegram.route_telegram_message import route_msg
from apps.telegram.utils import send_message
from utils.error_handle_decorators import async_error_processing
from utils.response_formatters import google_json_response

logger = logging.getLogger(__name__)


@async_error_processing(logger=logger)
async def handle_telegram_request(request):
    try:
        data = await request.json()
        incoming_message = TelegramIncomingMessage.from_telegram_dict(data)
        sc = SegmentsController.from_params(
            session=request.app.session, lobster_config=request.app.lobster_config
        )
        out = await route_msg(incoming_message, segments_controller=sc)
        await send_message(session=request.app.session, msg=out)
    except Exception as e:
        logger.exception(e)
    return google_json_response(data={'result': 'ok'}, status=200, api_version='1.0')
