import logging

from aiohttp import ClientSession

import settings
from apps.telegram.entities import TelegramOutgoingMessage

logger = logging.getLogger(__name__)


async def send_message(session: ClientSession, msg: TelegramOutgoingMessage):
    if settings.ENV == 'prod':
        async with session.post(f'https://api.telegram.org/bot{settings.BOT_TOKEN}/sendMessage',
                                json={
                                    'chat_id': msg.chat,
                                    'text': msg.text
                                }) as resp:
            if resp.status != 200:
                logger.exception({'msg': f'Не удалось отправить сообщение в тг',
                                  'text': await resp.text()}
                                 )
