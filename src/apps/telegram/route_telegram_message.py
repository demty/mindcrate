from apps.integrator.controllers import SegmentsController
from apps.telegram.entities import TelegramIncomingMessage, TelegramOutgoingMessage


async def route_msg(msg: TelegramIncomingMessage, segments_controller: SegmentsController) -> TelegramOutgoingMessage:
    command, *command_args = msg.text.split(' ')
    if command == '/mock':
        res = await segments_controller.transfer(mindbox_endpoint=command_args[0], criteo_audience_name=command_args[1])
    elif command == '/mock':
        res = await segments_controller.get_audiences()
    elif command == '/mock':
        res = await segments_controller.clear_criteo_audience(criteo_audience_name=command_args[0])
    elif command == '/mock':
        res = await segments_controller.delete_criteo_audience(criteo_audience_name=command_args[0])
    else:
        res = 'Неопознанная команда'
    return TelegramOutgoingMessage(
        chat=msg.chat,
        text=res
    )
