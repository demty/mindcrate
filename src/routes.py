import apps.integrator.views as faker_views
import apps.metrics.views as metric_views
import apps.telegram.views as telegram_views
import apps.tracker.views as tracker_views

routes = [
    ['GET', '/healthcheck', metric_views.MetricHandler.healthcheck],
    ('POST', '/{api_version}/track/cart', tracker_views.track_cart),
    ('POST', '/{api_version}/track/order/create', tracker_views.track_order_create),
    ('POST', '/{api_version}/track/wishlist', tracker_views.track_wishlist),
    ('POST', '/{api_version}/track/wishlist/delete', tracker_views.track_wishlist_remove),
    ('POST', '/{api_version}/track/order/update', tracker_views.update_order),
    ('POST', '/{api_version}/track/user/popup', tracker_views.register_user_from_popup),
    ('POST', '/{api_version}/track/user/register/popup', tracker_views.register_from_popmechanic),
    ('POST', '/{api_version}/track/user/email', tracker_views.subscribe_user_on_email),
    ('POST', '/{api_version}/track/user/register', tracker_views.register_user_on_site),
    ('POST', '/{api_version}/track/user/update', tracker_views.update_user_on_site),
    ('POST', '/{api_version}/track/user/update/subscription', tracker_views.subscription_update),
    ('POST', '/{api_version}/track/user/herb/register', tracker_views.register_herb_user),
    ('POST', '/{api_version}/track/user/auth', tracker_views.auth_user),
    ('POST', '/{api_version}/track/category/view', tracker_views.view_category),
    ('POST', '/{api_version}/track/product/view', tracker_views.view_product),
    ('POST', '/{api_version}/track/manual/admitad', tracker_views.forced_admitad_sync),
    ('POST', '/{api_version}/track/waitlist', tracker_views.track_waitlist_add),
    ('DELETE', '/{api_version}/track/waitlist', tracker_views.track_waitlist_remove),
    ('POST', '/{api_version}/track/manual/admitad', tracker_views.forced_admitad_sync),
    ('POST', '/{api_version}/google_analytics/measurement_protocol', tracker_views.google_analytics_request),
    ('DELETE', '/{api_version}/track/beloved-brands', tracker_views.track_remove_from_beloved_brands),
    ('POST', '/{api_version}/track/beloved-brands', tracker_views.track_add_to_beloved_brands),
    ('POST', '/{api_version}/fake/lists', faker_views.reset_wish_and_wait_lists),
    ('POST', '/{api_version}/fake/users', faker_views.update_users),
    ('POST', '/{api_version}/telegram', telegram_views.handle_telegram_request),
    # ('GET', '/test1', tracker_views.test1),
    # ('GET', '/test2', tracker_views.test2)
]


def setup_routes(app):
    """Подключить роуты"""
    for route in routes:
        app.router.add_route(*route)
